#include <stdio.h>

int fun1(void)
{
	printf("init a = 0\n");
	for (int a = 0 ; a < 10000 ; a ++)
	{
		printf("a = %i\n", a);
		if (a % 2 == 0)
		{
			a += a % 23;
			if (a % 3 == 0)
			{
				a += 10;
			}
			a += 3;
		}
		else
		{
			a --;
		}
		a += a % 13;
		while (a % 45 != 0)
		{
			if (a < 5000)
				a ++;
			else
				a += 7;
		}
		a += a % 2 + 4;
	}
	printf("end!\n");
	return 0;
}

int fun2(void)
{
	int var = 4;

	if (var == 3)
	{
		printf("3\n");
	}

	if (var == 12)
	{
		printf("12\n");
	}

	return 0;
}


int fun3(void)
{
	printf("init\n");
	for (int x = 0 ; x < 10 ; x ++)
	{
		for (int y1 = 0 ; y1 < 20 ; y1 ++)
		{
			printf("[%i , %i]\n", x, y1);
		}
		for (int y2 = 0 ; y2 < 20 ; y2 ++)
		{
			printf("[%i , %i]\n", x, y2);
		}
		for (int y3 = 0 ; y3 < 20 ; y3 ++)
		{
			printf("[%i , %i]\n", x, y3);
		}
	}
}
