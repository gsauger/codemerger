#!/bin/bash

set -e

for f in *.c; do
	../../merger parse "$f"
	echo --
done
