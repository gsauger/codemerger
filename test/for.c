#include <stdio.h>

int main(void)
{
	printf("init a = 0\n");
	for (int a = 0 ; a < 10000 ; a ++)
	{
		printf("a = %i\n", a);
		if (a % 2 == 0)
		{
			a += a % 23;
			if (a % 3 == 0)
			{
				a += 10;
			}
			a += 3;
		}
		else
		{
			a --;
		}
		a += a % 13;
		while (a % 45 != 0)
		{
			if (a < 5000)
				a ++;
			else
				a += 7;
		}
		a += a % 2 + 4;
	}
	printf("end!\n");
	return 0;
}
