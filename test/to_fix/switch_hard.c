#include <stdio.h>

int main (void)
{
	int var = 6;

	switch (var)
	{
		case 2:
		case 3:
		case 4:
		case 5:
			printf("var = 2 ou 3\n");

		case 6:
			printf("var = 6\n");
			var = 8;
			break;

		case 8:
			printf("var = 8\n");
			break;
	}
	return 0;
}
