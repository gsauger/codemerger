#include <stdio.h>

int main(void)
{
	int var = 4;

	do
	{
		printf("var = %i\n", var);
		var *= 2;
	} while (var <= 150);

	printf("fin\n");

	return 0;
}
