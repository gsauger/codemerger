import pandas as pd
import argparse

#parser=arparse.ArgumentParser()
#parser.add_argument("-p","--pairs")
#parser.add_argument("-s","--scores")
#args=parser.parse_args()


# Read the first CSV file with function pairs
#pairs_df = pd.read_csv(args.pairs)
pairs_df = pd.read_csv("results/pairs_result_GGSNN.csv")

# Read the second CSV file with scores
#scores_df = pd.read_csv(args.scores)
scores_df = pd.read_csv("results/selected_pairs.csv")

# Merge the two DataFrames based on the identifier columns
#merged_df = pd.merge(pairs_df, scores_df, on=['idb_path_1', 'func_name_1', 'idb_path_2', 'func_name_2'])
merge_1 = pd.merge(pairs_df, scores_df,  on=['idb_path_1', 'func_name_1', 'idb_path_2', 'func_name_2'])

df1_inverted = pairs_df.rename(columns={'func_name_1': 'func_name_2', 'idb_path_1': 'idb_path_2', 'idb_path_2': 'idb_path_1', 'func_name_2': 'func_name_1'})
merge_2 = pd.merge(df1_inverted, scores_df,on=['idb_path_1', 'func_name_1', 'idb_path_2', 'func_name_2'])

merged_df = pd.concat([merge_1, merge_2], ignore_index=True)

merged_df = merged_df.sort_values(by='idb_path_1')
# Display the merged DataFrame with isolated scores
merged_df['sim'] = -merged_df['sim'].round(5)
print(merged_df[['idb_path_1', 'func_name_1', 'idb_path_2', 'func_name_2','sim']])
merged_df[['idb_path_1', 'func_name_1', 'idb_path_2', 'func_name_2','sim']].to_csv("selected_pairs_scores.csv")
