CMP_DIR="/home/gabriel/codemerger/examples/test/source_and_target_parse/compiled"
RES_DIR="/home/gabriel/codemerger/examples/test/source_and_target_parse/results"
SRC_DIR="/home/gabriel/codemerger/examples/test/source_and_target_parse/source_code"
PAR_DIR="/home/gabriel/codemerger/examples/test/source_and_target_parse/parsed_source_code"
mkdir $CMP_DIR
mkdir $RES_DIR
rm $CMP_DIR/*
rm $RES_DIR/*
rm $PAR_DIR/*

# Parsing
echo "parsing..."

cd ~/codemerger
for file in $SRC_DIR/*
do
echo "$file:"
	./merger parse $file > $RES_DIR/tmp_parsed.c
	RET=$?
	if  [[ $RET -eq 0 ]] then
		cp $file $PAR_DIR/
		cp $RES_DIR/tmp_parsed.c $PAR_DIR/$(basename $file | cut -d . -f 1)_parsed.c
	fi
done

# pairs to be printed from the output
echo "Writing pairs to be printed"
OUT_SELECT=$RES_DIR/selected_pairs.csv
echo "idb_path_1,func_name_1,idb_path_2,func_name_2" > $OUT_SELECT
echo "IDBs/Dataset-Muaz/source.i64,main,IDBs/Dataset-Muaz/target.i64,main" >> $OUT_SELECT
echo "IDBs/Dataset-Muaz/source.i64,main,IDBs/Dataset-Muaz/source_parsed.i64,main" >> $OUT_SELECT
echo "IDBs/Dataset-Muaz/target.i64,main,IDBs/Dataset-Muaz/target_parsed.i64,main" >> $OUT_SELECT
echo "IDBs/Dataset-Muaz/source_parsed.i64,main,IDBs/Dataset-Muaz/target_parsed.i64,main" >> $OUT_SELECT
for file in $PAR_DIR/*
do
	./merger list $file > $RES_DIR/tmp_list.txt
	cat $RES_DIR/tmp_list.txt >> $RES_DIR/fun_of_interest.txt
	for fname in $(cat $RES_DIR/tmp_list.txt)
	do
		echo "IDBs/Dataset-Muaz/source.i64,$fname,IDBs/Dataset-Muaz/target.i64,$fname" >> $OUT_SELECT
	done
done

# compile the source file
echo "Compiling..."

for file in $PAR_DIR/*
do
	echo $(basename $file)
	gcc -O0 $file -o $CMP_DIR/$(basename $file | cut -d . -f 1)
done

# create new binary database to run the model on it
echo "Writing the binary database in the cisco fork"
rm ~/cisco_fork/Binaries/Dataset-Muaz/bins/*
rm ~/cisco_fork/IDBs/Dataset-Muaz/*
rm -r ~/cisco_fork/DBs/Dataset-Muaz/*
cp $CMP_DIR/* ~/cisco_fork/Binaries/Dataset-Muaz/bins/
cp $RES_DIR/fun_of_interest.txt ~/cisco_fork/IDA_scripts/dataset_creation_notebooks/

# preprocess the new binaries
echo "Processing the new database in the cisco fork"
cd ~/cisco_fork/IDA_scripts/
python3 generate_idbs.py --muaz
./generate_acfg_feature_from_idbs.sh

# run the model on the test data
echo "Running the models"
cd ../Models/GGSNN-GMN/
./preprocess_data_test.sh
./testing_script.sh

# copy the results in ~codemerger/results
echo "Copying the results"
cp NeuralNetwork/Dataset-Muaz/pairs_testing_Dataset-Muaz.csv $RES_DIR/pairs_result_GGSNN.csv
cd $RES_DIR/..
python3 get_scores.py
