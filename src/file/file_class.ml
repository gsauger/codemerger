open Utils
open Func_class

let parse code_buf = (* modify the lexer here *)
	try
		MenhirLib.Convert.Simplified.traditional2revised Parser.file
			(fun () ->
				let tok = Lexer.lex code_buf in
				let (pstart, pend) = Sedlexing.lexing_positions code_buf in
				let () = Logs.debug (fun m ->
					let pstart_c = pstart.Lexing.pos_cnum - pstart.Lexing.pos_bol in
					let pend_c = pend.Lexing.pos_cnum - pend.Lexing.pos_bol in
					let pos_string = Printf.sprintf "%i:%i -> %i:%i"
						pstart.Lexing.pos_lnum pstart_c pend.Lexing.pos_lnum pend_c in
						m "[In FILE parser] pos %s %stoken : %a%!" pos_string (Util.spaces (18 - (String.length pos_string))) Lexer.pp_tok tok) in
				(tok, pstart, pend))
	with
	| Parser.Error -> let (pstart, pend) = Sedlexing.lexing_positions code_buf in
		Error.e (Error.ParseError (pstart, pend))
	| e -> raise e

type file_construct =
	| Path of string
	| Channel of (In_channel.t * Lexing.position option)
	| String of (string * Lexing.position option)
	| CList of file_elem list

class file construct =
	let (path, c) =
		match construct with
		| Path path ->
			let in_ch =
				try
					In_channel.open_bin path
				with
				| _ -> Error.e (Error.NoSuchFile path) in
			let buf = Sedlexing.Utf8.from_channel in_ch in
			let () = Sedlexing.set_filename buf path in
			let fe_list = parse buf in
			(path, fe_list)
		| Channel (in_ch, None) ->
			let buf = Sedlexing.Utf8.from_channel in_ch in
			let () = Sedlexing.set_filename buf "<input>" in
			let fe_list = parse buf in
			("", fe_list)
		| Channel (in_ch, Some pos) ->
			let buf = Sedlexing.Utf8.from_channel in_ch in
			let () = Sedlexing.set_position buf pos in
			let () = Sedlexing.set_filename buf pos.pos_fname in
			let fe_list = parse buf in
			("", fe_list)
		| String (str, None) ->
			let buf = Sedlexing.Utf8.from_string str in
			let () = Sedlexing.set_filename buf "<input>" in
			let fe_list = parse buf in
			("", fe_list)
		| String (str, Some pos) ->
			let buf = Sedlexing.Utf8.from_string str in
			let () = Sedlexing.set_position buf pos in
			let () = Sedlexing.set_filename buf pos.pos_fname in
			let fe_list = parse buf in
			("", fe_list)
		| CList fe_list -> ("", fe_list) in

object (s)
	val path = path
	val content = (c : file_elem list)

	method get_fun name =
		let f = List.find_opt
			(fun e ->
				match e with
				| Func f -> f#get_name = name
				| Code _ -> false)
			content in
		match f with
		| Some (Func f) -> f
		| _ -> Error.e (Error.FunctionNotFound (path, name))

	method get_fun_names =
		List.filter_map
			(fun e ->
				match e with
				| Func f -> Some f#get_name
				| Code _ -> None)
			content

	method pp fmt pp_mode =
		let () = Format.fprintf fmt "@[<v>" in
		let global_code = Util.get_global_code () in
		let () =
			if global_code <> [] then
				(Format.fprintf fmt "%a@ " Print.Instr.pp_instructions global_code) in
		let () =
			match pp_mode with
			| Base ->
				List.iter (fun e ->
					match e with
					| Func f -> let _ = f#get_base in ()
					| Code _ -> ())
				content
			| Full ->
				List.iter (fun e ->
					match e with
					| Func f -> let _ = f#get_full in ()
					| Code _ -> ())
				content
			| Auto -> () in
			let () = List.iter (fun e ->
				match e with
				| Func f -> Format.fprintf fmt "%a@ " f#pp pp_mode
				| Code str -> Format.fprintf fmt "%s@ " str)
				content in
			Format.fprintf fmt "@]"

	method save_as pp_mode path =
		let out_ch =
			try
				Out_channel.open_bin path
			with
			| _ -> Error.e (Error.ImpossibleToWrite path) in
		let fmt = Format.formatter_of_out_channel out_ch in
		Format.fprintf fmt "%a%!" s#pp pp_mode

	method save pp_mode =
		s#save_as pp_mode path
end
