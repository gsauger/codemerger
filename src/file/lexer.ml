let lex buf =
	let rec lex_aux acc =
		match%sedlex buf with
		| '#', Star ((Sub (any, ('\n' | '/')) | ('/', Sub (any, '*')) | "\\\n"))
		| "//", Star (Sub (any, '\n')), '\n'
		| "/*", Star (Sub (any, '*') | '*', Sub (any, '/')) , "*/"
		| ' ' | '\t' | '\n' | '\013' | '\012' -> lex_aux (acc ^ Sedlexing.Utf8.lexeme buf)
		| "..." -> Parser.ELLIPSIS (acc ^ Sedlexing.Utf8.lexeme buf)
		| ">>=" -> Parser.RIGHT_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "<<=" -> Parser.LEFT_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "+=" -> Parser.ADD_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "-=" -> Parser.SUB_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "*=" -> Parser.MUL_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "/=" -> Parser.DIV_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "%=" -> Parser.MOD_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "&=" -> Parser.AND_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "^=" -> Parser.XOR_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "|=" -> Parser.OR_ASSIGN (acc ^ Sedlexing.Utf8.lexeme buf)
		| ">>" -> Parser.RIGHT_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "<<" -> Parser.LEFT_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "++" -> Parser.INC_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "--" -> Parser.DEC_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "->" -> Parser.PTR_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "&&" -> Parser.AND_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "||" -> Parser.OR_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "<=" -> Parser.LE_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| ">=" -> Parser.GE_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "==" -> Parser.EQUALEQUAL (acc ^ Sedlexing.Utf8.lexeme buf)
		| "!=" -> Parser.NE_OP (acc ^ Sedlexing.Utf8.lexeme buf)
		| "=" -> Parser.EQUAL (acc ^ Sedlexing.Utf8.lexeme buf)
		| "(" -> Parser.LPAREN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "[" -> Parser.LSQUARE (acc ^ Sedlexing.Utf8.lexeme buf)
		| "{" -> Parser.LBRACKET (acc ^ Sedlexing.Utf8.lexeme buf)
		| ")" -> Parser.RPAREN (acc ^ Sedlexing.Utf8.lexeme buf)
		| "]" -> Parser.RSQUARE (acc ^ Sedlexing.Utf8.lexeme buf)
		| "}" -> Parser.RBRACKET (acc ^ Sedlexing.Utf8.lexeme buf)
		| ";" -> Parser.SEMI (acc ^ Sedlexing.Utf8.lexeme buf)
		| "," -> Parser.COMMA (acc ^ Sedlexing.Utf8.lexeme buf)
		| ":" -> Parser.COLON (acc ^ Sedlexing.Utf8.lexeme buf)
		| "*" -> Parser.STAR (acc ^ Sedlexing.Utf8.lexeme buf)
		| "struct" -> Parser.STRUCT (acc ^ Sedlexing.Utf8.lexeme buf)
		| "enum" -> Parser.ENUM (acc ^ Sedlexing.Utf8.lexeme buf)
		| "union" -> Parser.UNION (acc ^ Sedlexing.Utf8.lexeme buf)
		| "void" -> Parser.VOID (acc ^ Sedlexing.Utf8.lexeme buf)
		| "typeof_unqual" -> Parser.TYPEOF_UNQUAL (acc ^ Sedlexing.Utf8.lexeme buf)
		| "typeof" -> Parser.TYPEOF (acc ^ Sedlexing.Utf8.lexeme buf)
		| "float" | "double" | "complex" | "char" | "short" | "int" | "signed" | "unsigned" | "long" | "__int8_t"
		| "__int16_t" | "__int32_t" | "__int64_t" | "__uint8_t" | "__uint16_t" | "__uint32_t" | "__uint64_t" -> Parser.ARITH_TYPE (acc ^ Sedlexing.Utf8.lexeme buf)
		| "typedef" | "auto" | "register" | "static" | "extern" -> Parser.STORAGE_CLASS_SPEC (acc ^ Sedlexing.Utf8.lexeme buf)
		| "const" | "volatile" | "restrict" | "__restrict" | "_Atomic" -> Parser.TYPE_QUAL (acc ^ Sedlexing.Utf8.lexeme buf)
		| "inline" | "noreturn" | "__inline" | "__noreturn" | "_Noreturn" -> Parser.FUNC_SPEC (acc ^ Sedlexing.Utf8.lexeme buf)
		| "__attribute__" -> Parser.ATTRIBUTE (acc ^ Sedlexing.Utf8.lexeme buf)
		| "__extension__" -> Parser.EXTENSION (acc ^ Sedlexing.Utf8.lexeme buf)
		| "__asm__" -> Parser.ASM (acc ^ Sedlexing.Utf8.lexeme buf)
		| "alignas" -> Parser.ALIGN_SPEC (acc ^ Sedlexing.Utf8.lexeme buf)
		| (id_start | '_'), (Star id_continue) -> Parser.ID (acc ^ Sedlexing.Utf8.lexeme buf, Sedlexing.Utf8.lexeme buf)
		| '"', Star ((Sub (any, ('"' | '\\'))) | '\\', any), '"' -> Parser.CODE (acc ^ Sedlexing.Utf8.lexeme buf)
	  | '\'', (any | ('\\', any, Star (Sub (any, '\'')))), '\'' -> Parser.CODE (Sedlexing.Utf8.lexeme buf)
		| '/' -> Parser.CODE (acc ^ Sedlexing.Utf8.lexeme buf)
		| Plus (Sub (any, (' ' | '\t' | '\n' | '\013' | '(' | ')' | '[' | ']' | ',' | '=' | '*' | '{' | '}' | ';' | ':' | '"' | '\'' | '/'))) -> Parser.CODE (acc ^ Sedlexing.Utf8.lexeme buf)
		| eof -> Parser.EOF (acc ^ Sedlexing.Utf8.lexeme buf)
		| _ -> assert false in
	lex_aux ""

let pp_tok fmt tok =
	let tok_str = match tok with
	| Parser.VOID data -> Printf.sprintf "VOID \"%s\"" data
	| Parser.UNION data -> Printf.sprintf "UNION \"%s\"" data
	| Parser.TYPE_QUAL data -> Printf.sprintf "TYPE_QUAL \"%s\"" data
	| Parser.TYPEOF_UNQUAL data -> Printf.sprintf "TYPEOF_UNQUAL \"%s\"" data
	| Parser.TYPEOF data -> Printf.sprintf "TYPEOF \"%s\"" data
	| Parser.ELLIPSIS data -> Printf.sprintf "ELLIPSIS \"%s\"" data
	| Parser.STRUCT data -> Printf.sprintf "STRUCT \"%s\"" data
	| Parser.STORAGE_CLASS_SPEC data -> Printf.sprintf "STORAGE_CLASS_SPEC \"%s\"" data
	| Parser.STATIC data -> Printf.sprintf "STATIC \"%s\"" data
	| Parser.STAR data -> Printf.sprintf "STAR \"%s\"" data
	| Parser.SEMI data -> Printf.sprintf "SEMI \"%s\"" data
	| Parser.RSQUARE data -> Printf.sprintf "RSQUARE \"%s\"" data
	| Parser.RPAREN data -> Printf.sprintf "RPAREN \"%s\"" data
	| Parser.RBRACKET data -> Printf.sprintf "RBRACKET \"%s\"" data
	| Parser.LSQUARE data -> Printf.sprintf "LSQUARE \"%s\"" data
	| Parser.LPAREN data -> Printf.sprintf "LPAREN \"%s\"" data
	| Parser.LBRACKET data -> Printf.sprintf "LBRACKET \"%s\"" data
	| Parser.ID (data1, data2) -> Printf.sprintf "ID \"%s\", \"%s\"" data1 data2
	| Parser.FUNC_SPEC data -> Printf.sprintf "FUNC_SPEC \"%s\"" data
	| Parser.RIGHT_ASSIGN data -> Printf.sprintf "RIGHT_ASSIGN \"%s\"" data
	| Parser.LEFT_ASSIGN data -> Printf.sprintf "LEFT_ASSIGN \"%s\"" data
	| Parser.ADD_ASSIGN data -> Printf.sprintf "ADD_ASSIGN \"%s\"" data
	| Parser.SUB_ASSIGN data -> Printf.sprintf "SUB_ASSIGN \"%s\"" data
	| Parser.MUL_ASSIGN data -> Printf.sprintf "MUL_ASSIGN \"%s\"" data
	| Parser.DIV_ASSIGN data -> Printf.sprintf "DIV_ASSIGN \"%s\"" data
	| Parser.MOD_ASSIGN data -> Printf.sprintf "MOD_ASSIGN \"%s\"" data
	| Parser.AND_ASSIGN data -> Printf.sprintf "AND_ASSIGN \"%s\"" data
	| Parser.XOR_ASSIGN data -> Printf.sprintf "XOR_ASSIGN \"%s\"" data
	| Parser.OR_ASSIGN data -> Printf.sprintf "OR_ASSIGN \"%s\"" data
	| Parser.RIGHT_OP data -> Printf.sprintf "RIGHT_OP \"%s\"" data
	| Parser.LEFT_OP data -> Printf.sprintf "LEFT_OP \"%s\"" data
	| Parser.INC_OP data -> Printf.sprintf "INC_OP \"%s\"" data
	| Parser.DEC_OP data -> Printf.sprintf "DEC_OP \"%s\"" data
	| Parser.PTR_OP data -> Printf.sprintf "PTR_OP \"%s\"" data
	| Parser.AND_OP data -> Printf.sprintf "AND_OP \"%s\"" data
	| Parser.OR_OP data -> Printf.sprintf "OR_OP \"%s\"" data
	| Parser.LE_OP data -> Printf.sprintf "LE_OP \"%s\"" data
	| Parser.GE_OP data -> Printf.sprintf "GE_OP \"%s\"" data
	| Parser.EQUALEQUAL data -> Printf.sprintf "EQUALEQUAL \"%s\"" data
	| Parser.NE_OP data -> Printf.sprintf "NE_OP \"%s\"" data
	| Parser.EQUAL data -> Printf.sprintf "EQUAL \"%s\"" data
	| Parser.EOF data -> Printf.sprintf "EOF \"%s\"" data
	| Parser.ENUM data -> Printf.sprintf "ENUM \"%s\"" data
	| Parser.ATTRIBUTE data -> Printf.sprintf "ATTRIBUTE \"%s\"" data
	| Parser.EXTENSION data -> Printf.sprintf "EXTENSION \"%s\"" data
	| Parser.ASM data -> Printf.sprintf "ASM \"%s\"" data
	| Parser.COMMA data -> Printf.sprintf "COMMA \"%s\"" data
	| Parser.COLON data -> Printf.sprintf "COLON \"%s\"" data
	| Parser.CODE data -> Printf.sprintf "CODE \"%s\"" data
	| Parser.ARITH_TYPE data -> Printf.sprintf "ARITH_TYPE \"%s\"" data
	| Parser.ALIGN_SPEC data -> Printf.sprintf "ALIGN_SPEC \"%s\"" data in
	let tok_str = String.fold_left (fun str c -> if c <> '\n' then str ^ (String.make 1 c) else str ^ "\\n") "" tok_str in
	Format.fprintf fmt "%s " tok_str
