(* data type to represent a file *)

let sum_8uple l1 l2 =
	let n1,n2,n3,n4,n5,n6,n7,n8 = l1 in
	let p1,p2,p3,p4,p5,p6,p7,p8 = l2 in
	n1+p1,n2+p2,n3+p3,n4+p4,n5+p5,n6+p6,n7+p7,n8+p8

let ifTuple = (1,0,0,0,0,0,0,0)
let elseTuple = (0,1,0,0,0,0,0,0)
let ifElseTuple = sum_8uple ifTuple elseTuple
let switchCaseTuple = (0,0,1,0,0,0,0,0)
let whileTuple = (0,0,0,1,0,0,0,0)
let doWhileTuple = (0,0,0,0,1,0,0,0)
let forTuple = (0,0,0,0,0,1,0,0)
let returnTuple = (0,0,0,0,0,0,1,0)
let breakContinueTuple = (0,0,0,0,0,0,0,1)
let zeroTuple = (0,0,0,0,0,0,0,0)

let rec count_cf_base_aux body =
	let _, cf = match body with Defs.Base.Prog(data, cf) -> data, cf in
	match cf with
	| Defs.Base.If (_, p1, p2) ->
		let tmp = sum_8uple (count_cf_base_aux p1) (count_cf_base_aux p2) in
		sum_8uple tmp ifTuple
	| Defs.Base.While (_, p1, p2) ->
		let tmp = sum_8uple (count_cf_base_aux p1) (count_cf_base_aux p2) in
		sum_8uple tmp whileTuple
	| Defs.Base.Return _ -> returnTuple
	| Defs.Base.Break
	| Defs.Base.Continue -> breakContinueTuple
	| Defs.Base.End -> zeroTuple

let rec count_cf_full_aux body =
	let _, cf = match body with Defs.Full.Prog(data, cf) -> data, cf in
	match cf with
	| Defs.Full.IfElse ((_, p1), p2l, p3o, p4) ->
			let tmp1 = sum_8uple ifTuple (count_cf_full_aux p1) in
			(* Next is for elseif) *)
			let tmp2 = List.fold_left (fun acc (_, p) -> sum_8uple (sum_8uple acc ifElseTuple) (count_cf_full_aux p)) tmp1 p2l in (* ask vincent if I don't remember what this is *)
			let tmp3 = match p3o with
				| Some p3 -> sum_8uple (sum_8uple tmp2 elseTuple) (count_cf_full_aux p3)
				| None -> tmp2 in
			let tmp4 = sum_8uple tmp3 (count_cf_full_aux p4) in
			tmp4
	| Defs.Full.Switch (_, p1l, p2o, p3) ->
			let tmp1 = List.fold_left (fun acc (_, p) -> sum_8uple (sum_8uple acc switchCaseTuple) (count_cf_full_aux p)) zeroTuple p1l in
			let tmp2 = match p2o with
				| Some p2 -> sum_8uple (sum_8uple tmp1 switchCaseTuple) (count_cf_full_aux p2)
				| None -> tmp1 in
			let tmp3 = sum_8uple tmp2 (count_cf_full_aux p3) in
			tmp3
	| Defs.Full.While (_, p1, p2) ->
			let tmp = sum_8uple (count_cf_full_aux p1) (count_cf_full_aux p2) in
			sum_8uple tmp whileTuple
	| Defs.Full.DoWhile (_, p1, p2) ->
			let tmp = sum_8uple (count_cf_full_aux p1) (count_cf_full_aux p2) in
			sum_8uple tmp doWhileTuple
	| Defs.Full.For (_, _, _, p1, p2) ->
			let tmp = sum_8uple (count_cf_full_aux p1) (count_cf_full_aux p2) in
			sum_8uple tmp forTuple
	| Defs.Full.Block (p1, p2) ->
			sum_8uple (count_cf_full_aux p1) (count_cf_full_aux p2)
	| Defs.Full.Return _ -> returnTuple
	| Defs.Full.Break
	| Defs.Full.Continue -> breakContinueTuple
	| Defs.Full.End -> zeroTuple

type pp_mode =
	| Auto
	| Base
	| Full

type func_construct =
	| Parse of Lexing.position * string
	| FullProg of Defs.Full.prog
	| BaseProg of Defs.Base.prog

class func name ret decl construct =
	let (name, ret, decl, pos, str_body, full_body, base_body) =
		match construct with
		| Parse (pos, code) ->
			(name, ret, decl, pos, Some code, None, None)
		| FullProg full_prog ->
			(name, ret, decl, Lexing.dummy_pos, None, Some full_prog, None)
		| BaseProg base_prog ->
			(name, ret, decl, Lexing.dummy_pos, None, None, Some base_prog) in

object (s)
	val name = (name : string)
	val ret = (ret : Defs.Types.ret_type)
	val decl = (decl : string)
	val pos = (pos : Lexing.position)
	val mutable str_body = (str_body : string option)
	val mutable full_body = (full_body : Defs.Full.prog option)
	val mutable base_body = (base_body : Defs.Base.prog option)

	method set_base body =
		if Some body != base_body then
			let () = str_body <- None in
			let () = full_body <- None in
			let () = base_body <- Some body in
				()

	method set_full body =
		if Some body != full_body then
			let () = str_body <- None in
			let () = full_body <- Some body in
			let () = base_body <- None in
				()

	method private compute_base =
		match base_body with
		| Some _ -> ()
		| None ->
			match full_body with
			| Some body -> base_body <- Some (Convert.Full.to_base body)
			| None ->
				match str_body with
				| Some str ->
					let fb = Parse.Full.parse pos str in
					let () = full_body <- Some fb in base_body <- Some (Convert.Full.to_base fb)
				| None -> assert false

	method private compute_full =
		match full_body with
		| Some _ -> ()
		| None ->
			match base_body with
			| Some body -> full_body <- Some (Convert.Base.to_full body)
			| None ->
				match str_body with
				| Some str -> full_body <- (Some (Parse.Full.parse pos str))
				| None -> assert false

	method get_name = name

	method get_ret = ret

	method get_number_cf printing_mode =
		match printing_mode with
		| Full ->
			let () = s#compute_full in
			(match full_body with
			| Some body ->
				count_cf_full_aux body
			| None -> failwith "not implemented")
		| Base ->
			let () = s#compute_base in
			(match base_body with
			| Some body ->
				count_cf_base_aux body
			| None -> failwith "not implemented")
		| _ -> failwith "not implemented"

	method get_base =
		let () = s#compute_base in
		match base_body with
		| Some body -> body
		| None -> failwith "not implemented"

	method get_full =
		let () = s#compute_full in
		match full_body with
		| Some body -> body
		| None -> failwith "not implemented"

	method pp fmt pp_mode =
		let () = Format.fprintf fmt "%s@ @[<v 2>{@ " decl in
		let () = match pp_mode with
		| Base -> let base = s#get_base in
			Print.Base.pp fmt base
		| Full -> let full = s#get_full in
			Print.Full.pp fmt full
		| Auto ->
			match base_body with
			| Some base -> Print.Base.pp fmt base
			| None ->
				match full_body with
				| Some full -> Print.Full.pp fmt full
				| None ->
					match str_body with
					| Some str -> Format.fprintf fmt "%s" str
					| None -> assert false in
			Format.fprintf fmt "@]@ }"
end

type file_elem =
	| Func of func
	| Code of string
