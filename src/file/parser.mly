%token<string> ALIGN_SPEC STATIC EOF LPAREN RPAREN SEMI STAR LSQUARE RSQUARE
LBRACKET RBRACKET EQUAL COMMA COLON STRUCT ENUM UNION VOID TYPEOF TYPEOF_UNQUAL
ELLIPSIS RIGHT_ASSIGN LEFT_ASSIGN ADD_ASSIGN SUB_ASSIGN MUL_ASSIGN DIV_ASSIGN
MOD_ASSIGN AND_ASSIGN XOR_ASSIGN OR_ASSIGN RIGHT_OP LEFT_OP INC_OP DEC_OP PTR_OP
AND_OP OR_OP LE_OP GE_OP EQUALEQUAL NE_OP CODE STORAGE_CLASS_SPEC TYPE_QUAL
FUNC_SPEC ARITH_TYPE ATTRIBUTE ASM EXTENSION
%token<string*string> ID

%start <Func_class.file_elem list> file (* axiom *)

%%

file:
| d = external_declaration d1 = file { d :: d1 }
| e = EOF { [ Func_class.Code e ] }

external_declaration:
| fun_def = function_definition { let (name, ret, decl, pos, body) = fun_def in Func_class.Func (new Func_class.func name ret decl (Func_class.Parse (pos, body))) }
| decl = declaration { Func_class.Code decl }

attribute_asm_list:
| a = attribute a2 = attribute_asm_list { a ^ a2 }
| a = asm a2 = attribute_asm_list { a ^ a2 }
| { "" }

attribute:
| a = ATTRIBUTE lp = LPAREN e = encapsulated_constant_expression rp = RPAREN { a ^ lp ^ e ^ rp }

asm:
| a = ASM lp = LPAREN e = encapsulated_constant_expression rp = RPAREN { a ^ lp ^ e ^ rp }

declaration:
| d = declaration_specifiers s = SEMI { let (d_code, _) = d in d_code ^ s }
| d = declaration_specifiers a1 = init_declarator_list s = SEMI { let (d_code, _) = d in d_code ^ a1 ^ s }

function_definition:
| ds = declaration_specifiers d = declarator cs = compound_statement { let (ds_code, ret) = ds in let (decl, name, ptr) = d in let (cs_code, cs_loc) = cs in (name, (if ptr then Defs.Types.Ptr else ret), ds_code ^ decl, cs_loc, cs_code) }
(* | d = declarator cs = compound_statement { let (decl, name) = d in (name, decl, fst ($loc(cs)), cs) } *)

declaration_specifiers:
| s = STORAGE_CLASS_SPEC d = declaration_specifiers { let (d_code, ret) = d in (s ^ d_code, ret) }
| t = TYPE_QUAL d = declaration_specifiers { let (d_code, ret) = d in (t ^ d_code, ret) }
| t = type_specifier t1 = declaration_specifiers_no_id { let (t_code, ret) = t in let (t1_code, _) = t1 in (t_code ^ t1_code, ret) }
| t = type_specifier { let (t_code, ret) = t in (t_code, ret) }
(* the two following lines are a workaround for not knowing which types are defined by the developper, because we don't always run the preprocessor on the source files *)
| id = ID d = declaration_specifiers_no_ts { let (id_text, _) = id in let (d_code, ret) = d in (id_text ^ d_code, ret) }
| id = ID { let (id_text, _) = id in (id_text, Defs.Types.Int) }
| fs = FUNC_SPEC d = declaration_specifiers { let (d_code, ret) = d in (fs ^ d_code, ret) }
| e = EXTENSION d = declaration_specifiers { let (d_code, ret) = d in (e ^ d_code, ret) }
| a = attribute d = declaration_specifiers { let (d_code, ret) = d in (a ^ d_code, ret) }

declaration_specifiers_no_id:
| s = STORAGE_CLASS_SPEC d = declaration_specifiers_no_id { let (d_code, ret) = d in (s ^ d_code, ret) }
| s = STORAGE_CLASS_SPEC { (s, Defs.Types.Int) }
| t = TYPE_QUAL d = declaration_specifiers_no_id { let (d_code, ret) = d in (t ^ d_code, ret) }
| t = TYPE_QUAL { (t, Defs.Types.Int) }
| t = type_specifier t1 = declaration_specifiers_no_id { let (t_code, ret) = t in let (t1_code, _) = t1 in (t_code ^ t1_code, ret) }
| t = type_specifier { let (t_code, ret) = t in (t_code, ret) }
| fs = FUNC_SPEC d = declaration_specifiers_no_id { let (d_code, ret) = d in (fs ^ d_code, ret) }
| fs = FUNC_SPEC { (fs, Defs.Types.Int) }
| e = EXTENSION d = declaration_specifiers_no_id { let (d_code, ret) = d in (e ^ d_code, ret) }
| e = EXTENSION { (e, Defs.Types.Int) }
| a = attribute d = declaration_specifiers_no_id { let (d_code, ret) = d in (a ^ d_code, ret) }
| a = attribute { (a, Defs.Types.Int) }

declaration_specifiers_no_ts:
| s = STORAGE_CLASS_SPEC d = declaration_specifiers_no_ts { let (d_code, ret) = d in (s ^ d_code, ret) }
| s = STORAGE_CLASS_SPEC { (s, Defs.Types.Int) }
| t = TYPE_QUAL d = declaration_specifiers_no_ts { let (d_code, ret) = d in (t ^ d_code, ret) }
| t = TYPE_QUAL { (t, Defs.Types.Int) }
| fs = FUNC_SPEC d = declaration_specifiers_no_ts { let (d_code, ret) = d in (fs ^ d_code, ret) }
| fs = FUNC_SPEC { (fs, Defs.Types.Int) }
| e = EXTENSION d = declaration_specifiers_no_id { let (d_code, ret) = d in (e ^ d_code, ret) }
| e = EXTENSION { (e, Defs.Types.Int) }
| a = attribute d = declaration_specifiers_no_id { let (d_code, ret) = d in (a ^ d_code, ret) }
| a = attribute { (a, Defs.Types.Int) }

init_declarator_list:
| id = init_declarator c = COMMA i = init_declarator_list { id ^ c ^ i }
| id = init_declarator { id }

init_declarator:
| d = declarator { let (decl, _, _) = d in decl }
| d = declarator e = EQUAL i = initializr { let (decl, _,  _) = d in decl ^ e ^ i }

type_specifier:
| v = VOID { (v, Defs.Types.Void) }
| a = ARITH_TYPE { (a, Defs.Types.Int) }
| s = struct_or_union_specifier { (s, Defs.Types.Int) }
| e = enum_specifier { (e, Defs.Types.Int) }

(* ---- struct *)
struct_or_union_specifier:
| s = struct_or_union id = ID lb = LBRACKET sdl = struct_declaration_list rb = RBRACKET { let (id_text, _) = id in s ^ id_text ^ lb ^ sdl ^ rb }
| s = struct_or_union lb = LBRACKET sdl = struct_declaration_list rb = RBRACKET { s ^ lb ^ sdl ^ rb }
| s = struct_or_union id = ID { let (id_text, _) = id in s ^ id_text }

struct_or_union:
| s = STRUCT { s }
| u = UNION { u }

struct_declaration_list:
| s = struct_declaration s1 = struct_declaration_list { s ^ s1 }
| s = struct_declaration { s }

struct_declaration:
| ds = declaration_specifiers sdl = struct_declarator_list s = SEMI { let (ds_code, _) = ds in ds_code ^  sdl ^ s }

struct_declarator_list:
| s = struct_declarator c = COMMA sdl = struct_declarator_list { s ^ c ^ sdl}
| s = struct_declarator { s }

struct_declarator:
| d = declarator { let (decl, _, _) = d in decl }
| c = COLON ce = constant_expression { c ^ ce }
| d = declarator c = COLON ce = constant_expression { let (decl, _, _) = d in decl ^ c ^ ce }

(* ---- enum *)
enum_specifier:
| e = ENUM lb = LBRACKET el = enumerator_list rb = RBRACKET { e ^ lb ^ el ^ rb }
| e = ENUM id = ID lb = LBRACKET el = enumerator_list rb = RBRACKET { let (id_text, _) = id in e ^ id_text ^ lb ^ el ^ rb }
| e = ENUM id = ID { let (id_text, _) = id in e ^ id_text }

enumerator_list:
| e = enumerator c = COMMA el = enumerator_list { e ^ c ^ el }
| e = enumerator { e }

enumerator:
| id = ID { let (id_text, _) = id in id_text }
| id = ID e = EQUAL c = constant_expression { let (id_text, _) = id in id_text ^ e ^ c }

declarator:
| p = pointer dd = direct_declarator a = attribute_asm_list { let (decl, name, _) = dd in (p ^ decl ^ a, name, true) }
| dd = direct_declarator a = attribute_asm_list { let (decl, ret, name) = dd in (decl ^ a, ret, name) }

direct_declarator:
| id = ID { let (decl, name) = id in (decl, name, false) }
| lp = LPAREN d = declarator rp = RPAREN { let (decl, name, _) = d in (lp ^ decl ^ rp, name, true) }
| d = direct_declarator ls = LSQUARE rs = RSQUARE { let (decl, name, _) = d in (decl ^ ls ^ rs, name, true) }
| d = direct_declarator ls = LSQUARE c = constant_expression rs = RSQUARE { let (decl, name, _) = d in (decl ^ ls ^ c ^ rs, name, true) }
| d = direct_declarator lp = LPAREN p = parameter_list rp = RPAREN { let (decl, name, ptr) = d in (decl ^ lp ^ p ^ rp, name, ptr) }
| d = direct_declarator lp = LPAREN rp = RPAREN { let (decl, name, ptr) = d in (decl ^ lp ^ rp, name, ptr) }
(* | d = direct_declarator LPAREN i = identifier_list RPAREN { let (decl, name) = d in (decl ^ "(" ^ i ^ ")", name) } *) (* old style fun def *)

pointer:
| s = STAR { s }
| s = STAR t = type_qual_list { s ^ t }
| s = STAR p = pointer { s ^ p }
| s = STAR t = type_qual_list p = pointer { s ^ t ^ p }

type_qual_list:
| t = TYPE_QUAL { t }
| tl = type_qual_list t = TYPE_QUAL { tl ^ t }

parameter_list:
| p = parameter_declaration c = COMMA pl = parameter_list { p ^ c ^ pl }
| p = parameter_declaration c = COMMA e = ELLIPSIS { p ^ c ^ e }
| p = parameter_declaration { p }

parameter_declaration:
| ds = declaration_specifiers d = declarator { let (ds_code, _) = ds in let (decl, _, _) = d in ds_code ^ decl }
| ds = declaration_specifiers ad = abstract_declarator { let (ds_code, _) = ds in ds_code ^ " " ^ ad } (* needed for pointer argument with no name *)
| ds = declaration_specifiers { let (ds_code, _) = ds in ds_code }

(*
identifier_list:
| id = ID COMMA i = identifier_list { id ^ "," ^ i }
| id = ID { id }
*) (* old style fun def *)

(* ---- abstract declarator stuff *)

abstract_declarator:
| p = pointer { p }
| d = direct_abstract_declarator { d }
| p = pointer d = direct_abstract_declarator { p ^ d }

direct_abstract_declarator:
| LPAREN a = abstract_declarator RPAREN { "(" ^ a ^ ")" }
| LSQUARE RSQUARE { "[]" }
| LSQUARE c = constant_expression RSQUARE  { "[" ^ c ^ "]" }
| d = direct_abstract_declarator LSQUARE RSQUARE { d ^ "[]" }
| d = direct_abstract_declarator LSQUARE c = constant_expression RSQUARE { d ^ "[" ^ c ^ "]" }
| LPAREN RPAREN { "()" }
(* | LPAREN p = parameter_list RPAREN { "(" ^ p ^ ")" } *)
| d = direct_abstract_declarator LPAREN RPAREN { d ^ "()" }
| d = direct_abstract_declarator LPAREN p = parameter_list RPAREN { d ^ "(" ^ p ^ ")" }

(* ---- initializer *)

initializr:
| a = assignment_expression { a }
| lb = LBRACKET i = encapsulated_assignment_expression rb = RBRACKET { lb ^ i ^ rb }

(*
encapsulated_initializer:
| a = encapsulated_assignment_expression { a }
| lb = LBRACKET i = initializer_list rb = RBRACKET { lb ^ i ^ rb }

initializer_list:
| i = encapsulated_initializer c = COMMA il = initializer_list  { i ^ c ^ il }
| i = encapsulated_initializer c = COMMA { i ^ c }
| i = encapsulated_initializer { i }
*)

(* ---- function *)

compound_statement:
| _lb = LBRACKET f = function_body rb = RBRACKET { ("{" ^ f ^ rb, (fst $loc(_lb))) }

function_body:
| t = fun_token f = function_body { t ^ f }
| lb = LBRACKET f = function_body rb = RBRACKET f2 = function_body { lb ^ f ^ rb ^ f2 }
| { "" }

fun_token:
| id = ID { let (id_text, _) = id in id_text }
| code = CODE { code }
| s = STORAGE_CLASS_SPEC { s }
| t = TYPE_QUAL { t }
| f = FUNC_SPEC { f }
| t = ALIGN_SPEC { t }
| t = STATIC { t }
| t = LPAREN { t }
| t = RPAREN { t }
| t = SEMI { t }
| t = STAR { t }
| t = LSQUARE { t }
| t = RSQUARE { t }
| t = EQUAL { t }
| t = RIGHT_ASSIGN { t }
| t = LEFT_ASSIGN { t }
| t = ADD_ASSIGN { t }
| t = SUB_ASSIGN { t }
| t = MUL_ASSIGN { t }
| t = DIV_ASSIGN { t }
| t = MOD_ASSIGN { t }
| t = AND_ASSIGN { t }
| t = XOR_ASSIGN { t }
| t = OR_ASSIGN { t }
| t = RIGHT_OP { t }
| t = LEFT_OP { t }
| t = INC_OP { t }
| t = DEC_OP { t }
| t = PTR_OP { t }
| t = AND_OP { t }
| t = OR_OP { t }
| t = LE_OP { t }
| t = GE_OP { t }
| t = EQUALEQUAL { t }
| t = NE_OP { t }
| t = COMMA { t }
| t = COLON { t }
| t = STRUCT { t }
| t = ENUM { t }
| t = UNION { t }
| t = VOID { t }
| a = ARITH_TYPE { a }
| t = TYPEOF { t }
| t = TYPEOF_UNQUAL { t }
| t = ELLIPSIS { t }
| a = ASM { a }
| a = ATTRIBUTE { a }
| e = EXTENSION { e }

(* ---- expressions *)

constant_expression:
| t = expr_token c = constant_expression { t ^ c }
| ls = LSQUARE c = encapsulated_constant_expression rs = RSQUARE c2 = constant_expression { ls ^ c ^ rs ^ c2 }
| ls = LSQUARE c = encapsulated_constant_expression rs = RSQUARE { ls ^ c ^ rs }
| t = expr_token lb = LBRACKET c = encapsulated_constant_expression rb = RBRACKET c2 = constant_expression { t ^ lb ^ c ^ rb ^ c2 }
| t = expr_token lb = LBRACKET c = encapsulated_constant_expression rb = RBRACKET { t ^ lb ^ c ^ rb }
| lp = LPAREN c = encapsulated_constant_expression rp = RPAREN c2 = constant_expression { lp ^ c ^ rp ^ c2 }
| lp = LPAREN c = encapsulated_constant_expression rp = RPAREN { lp ^ c ^ rp }
| t = expr_token { t }

assignment_expression:
| c = constant_expression { c }

encapsulated_constant_expression:
| t = enscapsulated_expr_token c = encapsulated_constant_expression { t ^ c }
| ls = LSQUARE c = encapsulated_constant_expression rs = RSQUARE c2 = encapsulated_constant_expression { ls ^ c ^ rs ^ c2 }
| lb = LBRACKET c = encapsulated_constant_expression rb = RBRACKET c2 = encapsulated_constant_expression { lb ^ c ^ rb ^ c2 }
| lp = LPAREN c = encapsulated_constant_expression rp = RPAREN c2 = encapsulated_constant_expression { lp ^ c ^ rp ^ c2 }
| { "" }

encapsulated_assignment_expression:
| c = encapsulated_constant_expression { c }

enscapsulated_expr_token:
| e = expr_token { e }
| c = COMMA { c }
| s = SEMI { s }

expr_token:
| id = ID { let (id_text, _) = id in id_text }
| code = CODE { code }
| s = STORAGE_CLASS_SPEC { s }
| t = TYPE_QUAL { t }
| f = FUNC_SPEC { f }
| t = ALIGN_SPEC { t }
| t = STATIC { t }
| t = STAR { t }
| t = EQUAL { t }
| t = RIGHT_ASSIGN { t }
| t = LEFT_ASSIGN { t }
| t = ADD_ASSIGN { t }
| t = SUB_ASSIGN { t }
| t = MUL_ASSIGN { t }
| t = DIV_ASSIGN { t }
| t = MOD_ASSIGN { t }
| t = AND_ASSIGN { t }
| t = XOR_ASSIGN { t }
| t = OR_ASSIGN { t }
| t = RIGHT_OP { t }
| t = LEFT_OP { t }
| t = INC_OP { t }
| t = DEC_OP { t }
| t = PTR_OP { t }
| t = AND_OP { t }
| t = OR_OP { t }
| t = LE_OP { t }
| t = GE_OP { t }
| t = EQUALEQUAL { t }
| t = NE_OP { t }
| t = COLON { t }
| t = STRUCT { t }
| t = ENUM { t }
| t = UNION { t }
| t = VOID { t }
| a = ARITH_TYPE { a }
| t = TYPEOF { t }
| t = TYPEOF_UNQUAL { t }
| e = ELLIPSIS { e }
| a = ASM { a }
| a = ATTRIBUTE { a }
| e = EXTENSION { e }
