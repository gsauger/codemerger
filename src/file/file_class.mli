open Func_class

type file_construct =
	| Path of string
	| Channel of (In_channel.t * Lexing.position option)
	| String of (string * Lexing.position option)
	| CList of file_elem list

class file : file_construct -> object
	method get_fun : string -> func
	method get_fun_names : string list
	method pp : Format.formatter -> pp_mode -> unit
	method save_as : pp_mode -> string -> unit
	method save : pp_mode -> unit
end
