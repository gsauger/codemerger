type pp_mode =
	| Auto
	| Base
	| Full

type func_construct =
	| Parse of Lexing.position * string
	| FullProg of Defs.Full.prog
	| BaseProg of Defs.Base.prog

class func : string -> Defs.Types.ret_type -> string -> func_construct -> object
	method set_base : Defs.Base.prog -> unit
	method set_full : Defs.Full.prog -> unit
	method get_name : string
	method get_ret : Defs.Types.ret_type
	method get_base : Defs.Base.prog
	method get_full : Defs.Full.prog
	method pp : Format.formatter -> pp_mode -> unit
	method get_number_cf : pp_mode -> int * int * int * int * int * int * int * int
end

type file_elem =
	| Func of func
	| Code of string
