let rec to_full prog =
	let p2_data, p2_cf2 = match prog with Defs.Base.Prog (p2_data, p2_cf2) -> (p2_data, p2_cf2) in
	let new_cf =
		match p2_cf2 with
		| Defs.Base.End -> Defs.Full.End
		| Defs.Base.Return inst -> Defs.Full.Return inst
		| Defs.Base.If (cond, if_body, if_tail) -> Defs.Full.IfElse ((cond, to_full if_body), [], None, to_full if_tail)
		| Defs.Base.While (cond, while_body, while_tail) -> Defs.Full.While (cond, to_full while_body, to_full while_tail)
		| Defs.Base.Break -> Defs.Full.Break
		| Defs.Base.Continue -> Defs.Full.Continue in
	Defs.Full.Prog (p2_data, new_cf)
