open Utils
open Defs

type intermediate_cf =
	| I_Block of intermediate_prog * intermediate_prog
	| I_If of Defs.Instr.instruction * intermediate_prog * intermediate_prog
	| I_While of Defs.Instr.instruction * intermediate_prog * intermediate_prog
	| I_Return of Defs.Instr.instruction
	| I_Break
	| I_Continue
	| I_End

and intermediate_prog =
	| I_Prog of Defs.Instr.instructions * intermediate_cf

let rec to_intermediate_prog prog begin_inject continue_inject end_inject =
	let p2_data, p2_cf2 = match prog with Full.Prog (p2_data, p2_cf2) -> (begin_inject @ p2_data, p2_cf2) in
	to_intermediate_cf p2_data p2_cf2 continue_inject end_inject

and to_intermediate_cf p2_data p2_cf2 continue_inject end_inject =
	match p2_cf2 with
	| Full.End ->
		I_Prog (p2_data @ end_inject, I_End)
	| Full.Continue ->
		I_Prog (p2_data @ continue_inject, I_Continue)
	| Full.Break ->
		I_Prog (p2_data, I_Break)
	| Full.Return inst -> I_Prog (p2_data, I_Return inst)
	| Full.Block (body, tail) -> I_Prog (p2_data, I_Block (to_intermediate_prog body [] continue_inject [], to_intermediate_prog tail [] continue_inject end_inject))
	| Full.While (cond, body, tail) ->
		I_Prog (p2_data, I_While (cond, to_intermediate_prog body [] [] [], to_intermediate_prog tail [] continue_inject end_inject))
	| Full.DoWhile (cond, body, tail) ->
		let new_var = Util.generate_new_id () in
		let new_data = p2_data @ [ (Printf.sprintf "int %s = 1" new_var) ] in
		let new_cond = Printf.sprintf "!(!(%s) & !(%s))" (* or *) new_var cond in
		let inject = [ (Printf.sprintf "%s = 0" new_var) ] in
		I_Prog (new_data, I_While (new_cond, to_intermediate_prog body inject [] [], to_intermediate_prog tail [] continue_inject end_inject))
	| Full.IfElse (if_, elseif_list, else_opt, tail) ->
		let else_base_opt = Option.map (fun prog -> to_intermediate_prog prog [] continue_inject []) else_opt in
		let f data tail (cond, prog) new_prog_opt =
			let new_var = Util.generate_new_id () in
			let new_data = data @ [ (Printf.sprintf "long long %s = (long long) (%s)" new_var cond) ] in
			let new_tail =
				match new_prog_opt with
				| Some new_prog ->
					let not_cond = Printf.sprintf "!%s" new_var in
					I_Prog ([], I_If (not_cond, new_prog, tail))
				| None -> tail in
			Some (I_Prog (new_data, I_If (new_var, to_intermediate_prog prog [] continue_inject [], new_tail))) in
		let if_list = List.fold_right (f [] (I_Prog ([], I_End))) elseif_list else_base_opt in
		let new_tail = to_intermediate_prog tail [] continue_inject end_inject in
		let res = f p2_data new_tail if_ if_list in
		(match res with
		| Some res -> res
		| None -> assert false)
	| Full.For (init, cond, incr, body, tail) ->
		let new_data =
			match init with
			| "" -> []
			| str -> [ str ] in
		let insert_incr =
			match incr with
			| "" -> []
			| str -> [ str ] in
		let new_cond =
			match cond with
			| "" -> "1"
			| str -> str in
		I_Prog (p2_data, I_Block (I_Prog (new_data, I_While (new_cond, to_intermediate_prog body [] insert_incr insert_incr, I_Prog ([], I_End))), to_intermediate_prog tail [] continue_inject end_inject))
	| Full.Switch (exp, case_list, default_opt, tail) ->
		let new_var = Util.generate_new_id () in
		let new_data = p2_data @ [ (Printf.sprintf "long long %s = (long long) (%s)" new_var exp) ] in
		let new_tail = to_intermediate_prog tail [] continue_inject end_inject in
		let default_base_opt = Option.map (fun prog -> to_intermediate_prog prog [] continue_inject []) default_opt in
		match case_list with
		| [] ->
			let new_data_2 = new_data @ [ (Printf.sprintf "%s = %s" new_var new_var) ] in
			(match default_base_opt with
			| Some default_base ->
				I_Prog (new_data_2, I_Block (default_base, new_tail))
			| None -> to_intermediate_prog tail new_data_2 continue_inject end_inject)
		| first_case :: case_list_2 ->
			let f data tail (cond_list, body) new_prog_opt =
				let new_cond =
					List.fold_left (fun cond case_cond ->
						match cond with
						| "" -> Printf.sprintf "(%s == %s)" new_var case_cond
						| str -> Printf.sprintf "%s | (%s == %s)" str new_var case_cond) "" cond_list in
				let new_var_cond = Util.generate_new_id () in
				let new_data = data @ [ (Printf.sprintf "int %s = %s" new_var_cond new_cond) ] in
				let new_tail =
					match new_prog_opt with
					| Some new_prog ->
						let not_cond = Printf.sprintf "!%s" new_var_cond in
						I_Prog ([], I_If (not_cond, new_prog, tail))
					| None -> tail in
				Some (I_Prog (new_data, I_If (new_var_cond, to_intermediate_prog body [] continue_inject [], new_tail))) in
			let if_list = List.fold_right (f [] (I_Prog ([], I_End))) case_list_2 default_base_opt in
			let res = f new_data new_tail first_case if_list in
			(match res with
			| Some res -> res
			| None -> assert false)

let insert_at_end_of_tail p insert =
	let rec insert_at_end_of_tail_aux p =
		let p_data, p_cf = match p with I_Prog (p_data, p_cf) -> p_data, p_cf in
		match p_cf with
		| I_Block (body, tail) ->
			I_Prog (p_data, I_Block (body, insert_at_end_of_tail_aux tail))
		| I_If (cond, body, tail) ->
			I_Prog (p_data, I_If (cond, body, insert_at_end_of_tail_aux tail))
		| I_While (cond, body, tail) ->
			I_Prog (p_data, I_While (cond, body, insert_at_end_of_tail_aux tail))
		| I_Continue
		| I_Break
		| I_Return _ -> p
		| I_End ->
			let insert_data, insert_cf =
				match insert with I_Prog (insert_data, insert_cf) -> insert_data, insert_cf in
			I_Prog (p_data @ insert_data, insert_cf) in
	insert_at_end_of_tail_aux p

let rec remove_blocks p =
	let p_data, p_cf = match p with I_Prog (p_data, p_cf) -> p_data, p_cf in
	match p_cf with
	| I_Block (body, tail) ->
		let new_p_data, new_p_cf = match insert_at_end_of_tail body tail with I_Prog (p_data, p_cf) -> p_data, p_cf in
		remove_blocks (I_Prog (p_data @ new_p_data, new_p_cf))
	| I_If (cond, body, tail) ->
		Base.Prog (p_data, Base.If (cond, remove_blocks body, remove_blocks tail))
	| I_While (cond, body, tail) ->
		Base.Prog (p_data, Base.While (cond, remove_blocks body, remove_blocks tail))
	| I_Continue -> Base.Prog (p_data, Base.Continue)
	| I_Break -> Base.Prog (p_data, Base.Break)
	| I_Return i -> Base.Prog (p_data, Base.Return i)
	| I_End -> Base.Prog (p_data, Base.End)

let to_base prog =
	let intermediate_prog = to_intermediate_prog prog [] [] [] in
	let base_prog = remove_blocks intermediate_prog in
	base_prog
