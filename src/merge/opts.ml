let compute_value opt no_opt str no_str =
	match (opt, no_opt) with
		| true, true -> Utils.Error.e (Utils.Error.IncompatibleOptions (str, no_str))
		| true, false -> Some true
		| false, true -> Some false
		| false, false -> None

type options = {
	dummy_control_structs : bool;
	if_while_translation : bool;
	useless_continues : bool;
}
type options_temp = {
	dummy_control_structs_o : bool option;
	if_while_translation_o : bool option;
	useless_continues_o : bool option;
}
let options_conf def_val =
	[ ("dummy_control_structs", Config.Conf.Bool (Some def_val))
	; ("if_while_translation", Config.Conf.Bool (Some def_val))
	; ("useless_continues", Config.Conf.Bool (Some def_val)) ]

type options_target_modif = {
	symmetry : bool;
	insert_block_ids : bool;
}
type options_target_modif_temp = {
	symmetry_o : bool option;
	insert_block_ids_o : bool option;
}
let options_target_modif_conf def_val =
	[ ("symmetry", Config.Conf.Bool (Some def_val))
	; ("insert_block_ids", Config.Conf.Bool (Some def_val)) ]

type requirements = {
	weak_loop_control_instrs : bool;
	weak_return : bool;
	weak_control_structs : bool;
}
type requirements_temp = {
	weak_loop_control_instrs_o : bool option;
	weak_return_o : bool option;
	weak_control_structs_o : bool option;
}
let requirements_conf def_val =
	[ ("weak_loop_control_instrs", Config.Conf.Bool (Some def_val))
	; ("weak_return", Config.Conf.Bool (Some def_val))
	; ("weak_control_structs", Config.Conf.Bool (Some def_val)) ]

let pair_options all really_all
	dummy_control_structs no_dummy_control_structs
	symmetry no_symmetry
	if_while_translation no_if_while_translation
	useless_continues no_useless_continues
	insert_block_ids no_insert_block_ids =
	(all, really_all,
	dummy_control_structs, no_dummy_control_structs,
	symmetry, no_symmetry,
	if_while_translation, no_if_while_translation,
	useless_continues, no_useless_continues,
	insert_block_ids, no_insert_block_ids)

let merge_options (all, really_all,
	dummy_control_structs, no_dummy_control_structs,
	symmetry, no_symmetry,
	if_while_translation, no_if_while_translation,
	useless_continues, no_useless_continues,
	insert_block_ids, no_insert_block_ids) =
	let insert_block_ids_o = compute_value insert_block_ids no_insert_block_ids "id" "no-id" in
	if really_all then
		if no_dummy_control_structs then Utils.Error.e (Utils.Error.IncompatibleOptions ("All", "no-dummy-cs"))
		else if no_symmetry then Utils.Error.e (Utils.Error.IncompatibleOptions ("All", "no-symmetry"))
		else if no_if_while_translation then Utils.Error.e (Utils.Error.IncompatibleOptions ("All", "no-if-while-tr"))
		else if no_useless_continues then Utils.Error.e (Utils.Error.IncompatibleOptions ("All", "no-useless-cont"))
		else
			{ dummy_control_structs_o = Some true
			; if_while_translation_o = Some true
			; useless_continues_o = Some true },
			{ symmetry_o = Some true
			; insert_block_ids_o = insert_block_ids_o }
	else
		let symmetry_o = compute_value symmetry no_symmetry "symmetry" "no-symmetry" in
	if all then
		if no_dummy_control_structs then Utils.Error.e (Utils.Error.IncompatibleOptions ("all", "no-dummy-cs"))
		else if no_if_while_translation then Utils.Error.e (Utils.Error.IncompatibleOptions ("all", "no-if-while-tr"))
		else if no_useless_continues then Utils.Error.e (Utils.Error.IncompatibleOptions ("all", "no-useless-cont"))
		else
			{ dummy_control_structs_o = Some true
			; if_while_translation_o = Some true
			; useless_continues_o = Some true },
			{ symmetry_o
			; insert_block_ids_o }
	else
		let dummy_control_structs_o = compute_value dummy_control_structs no_dummy_control_structs "dummy-cs" "no-dummy-cs" in
		let if_while_translation_o = compute_value if_while_translation no_if_while_translation "if-while-tr" "no-if-while-tr" in
		let useless_continues_o = compute_value useless_continues no_useless_continues "useless-cont" "no-useless-cont" in
			{ dummy_control_structs_o
			; if_while_translation_o
			; useless_continues_o },
			{ symmetry_o
			; insert_block_ids_o }

let pair_requirements
	weak_loop_control_instrs no_weak_loop_control_instrs
	weak_return no_weak_return
	weak_control_structs no_weak_control_structs =
	(weak_loop_control_instrs, no_weak_loop_control_instrs,
	weak_return, no_weak_return,
	weak_control_structs, no_weak_control_structs)

let merge_requirements
	(weak_loop_control_instrs, no_weak_loop_control_instrs,
	weak_return, no_weak_return,
	weak_control_structs, no_weak_control_structs) =
	let weak_loop_control_instrs_o = compute_value weak_loop_control_instrs no_weak_loop_control_instrs "weak-lc" "no-weak-lc" in
	let weak_return_o = compute_value weak_return no_weak_return "weak-ret" "no-weak-ret" in
	let weak_control_structs_o = compute_value weak_control_structs no_weak_control_structs "weak-cs" "no-weak-cs" in
		{ weak_loop_control_instrs_o
		; weak_return_o
		; weak_control_structs_o }

let options_t =
	let all = Cmdliner.Arg.(value & flag & info [ "a"; "all" ] ~doc:"Enable all merging options, except symmetry.") in
	let really_all = Cmdliner.Arg.(value & flag & info [ "A"; "All" ] ~doc:"Enable all merging options, including symmetry.") in
	let dummy_control_structs = Cmdliner.Arg.(value & flag & info [ "dummy-cs" ]
		~doc:"Enable dummy control structs merging option, which allows to create dummy control structs in the source to match the target.") in
	let no_dummy_control_structs = Cmdliner.Arg.(value & flag & info [ "no-dummy-cs" ]
		~doc:"Disable dummy control structs merging option, default state.") in
	let symmetry = Cmdliner.Arg.(value & flag & info [ "symmetry" ]
		~doc:"Enable symmetry merging option, which allows to also modify the target when it is impossible to match otherwise.") in
	let no_symmetry = Cmdliner.Arg.(value & flag & info [ "no-symmetry" ]
		~doc:"Disable symmetry merging option, default state.") in
	let if_while_translation = Cmdliner.Arg.(value & flag & info [ "if-while-tr" ]
		~doc:"Enable if while translation merging option, which allows to transform an if in the source to a while when there is a while in the target.") in
	let no_if_while_translation = Cmdliner.Arg.(value & flag & info [ "no-if-while-tr" ]
		~doc:"Disable if while translation merging option, default state.") in
	let useless_continues = Cmdliner.Arg.(value & flag & info [ "useless-cont" ]
		~doc:"Enable useless continues merging option, which allows to add or remove useless continues (at the end of a loop) in the source.") in
	let no_useless_continues = Cmdliner.Arg.(value & flag & info [ "no-useless-cont" ]
		~doc:"Disable useless continues merging option, default state.") in
	let insert_block_ids = Cmdliner.Arg.(value & flag & info [ "id" ]
		~doc:"Insert corresponding ids at the beginning of each corresponding blocks of the source and the target.") in
	let no_insert_block_ids = Cmdliner.Arg.(value & flag & info [ "no-id" ]
		~doc:"Disable ids insertion, default state.") in
	Cmdliner.Term.(const pair_options $ all $ really_all
		$ dummy_control_structs $ no_dummy_control_structs
		$ symmetry $ no_symmetry
		$ if_while_translation $ no_if_while_translation
		$ useless_continues $ no_useless_continues
		$ insert_block_ids $ no_insert_block_ids)

let requirements_t =
	let weak_loop_control_instrs = Cmdliner.Arg.(value & flag & info [ "weak-lc" ]
		~doc:"Enable weak loop control instructions merging option, which allows break and continue to match with themselves and end and return.") in
	let no_weak_loop_control_instrs = Cmdliner.Arg.(value & flag & info [ "no-weak-lc" ]
		~doc:"Disable weak loop control instructions merging option, default state.") in
	let weak_return = Cmdliner.Arg.(value & flag & info [ "weak-ret" ]
		~doc:"Enable weak return merging option, which allows return to match with break, continue and end.") in
	let no_weak_return = Cmdliner.Arg.(value & flag & info [ "no-weak-ret" ]
		~doc:"Disable weak return merging option, default state.") in
	let weak_control_structs = Cmdliner.Arg.(value & flag & info [ "weak-cf" ]
		~doc:"Enable weak control structs merging option, which allows if and while to match themselves.") in
	let no_weak_control_structs = Cmdliner.Arg.(value & flag & info [ "no-weak-cf" ]
		~doc:"Disable weak control structs merging option, default state.") in
	Cmdliner.Term.(const pair_requirements
	$ weak_loop_control_instrs $ no_weak_loop_control_instrs
	$ weak_return $ no_weak_return
	$ weak_control_structs $ no_weak_control_structs)

let merge_options_config c options =
	let dummy_control_structs =
		let (def_val, _) = Config.Conf.get_val_bool c "dummy_control_structs" in
		match options.dummy_control_structs_o with
		| Some value -> value
		| None -> def_val in
	let if_while_translation =
		let (def_val, _) = Config.Conf.get_val_bool c "if_while_translation" in
		match options.if_while_translation_o with
		| Some value -> value
		| None -> def_val in
	let useless_continues =
		let (def_val, _) = Config.Conf.get_val_bool c "useless_continues" in
		match options.useless_continues_o with
		| Some value -> value
		| None -> def_val in
	{ dummy_control_structs
	; if_while_translation
	; useless_continues }

let merge_options_target_modif_config c options_target_modif =
	let symmetry =
		let (def_val, _) = Config.Conf.get_val_bool c "symmetry" in
		match options_target_modif.symmetry_o with
		| Some value -> value
		| None -> def_val in
	let insert_block_ids =
		let (def_val, _) = Config.Conf.get_val_bool c "insert_block_ids" in
		match options_target_modif.insert_block_ids_o with
		| Some value -> value
		| None -> def_val in
	{ symmetry
	; insert_block_ids }

let merge_requirements_config c requirements =
	let weak_loop_control_instrs =
		let (def_val, _) = Config.Conf.get_val_bool c "weak_loop_control_instrs" in
		match requirements.weak_loop_control_instrs_o with
		| Some value -> value
		| None -> def_val in
	let weak_return =
		let (def_val, _) = Config.Conf.get_val_bool c "weak_return" in
		match requirements.weak_return_o with
		| Some value -> value
		| None -> def_val in
	let weak_control_structs =
		let (def_val, _) = Config.Conf.get_val_bool c "weak_control_structs" in
		match requirements.weak_control_structs_o with
		| Some value -> value
		| None -> def_val in
	{ weak_loop_control_instrs
	; weak_return
	; weak_control_structs }

let merge_all_aux c options_o options_target_modif_o requirements_o =
		(merge_options_config c options_o,
		merge_options_target_modif_config c options_target_modif_o,
		merge_requirements_config c requirements_o)

let get_all (path_o, options_t, requirements_t) =
	let (options, options_target_modif) = merge_options options_t in
	let requirements = merge_requirements requirements_t in
	let defs = Config.Conf.init (options_conf false @ options_target_modif_conf false @ requirements_conf false) in
	let c =
		match path_o with
		| Some path -> Config.Conf.parse_find defs path 0
		| None -> Config.Conf.parse_defs defs in
	merge_all_aux c options options_target_modif requirements

let get_requirements (path_o, requirements_t) =
	let requirements = merge_requirements requirements_t in
	let defs = Config.Conf.init (options_conf false @ options_target_modif_conf false @ requirements_conf false) in
	let c =
		match path_o with
		| Some path -> Config.Conf.parse_find defs path 0
		| None -> Config.Conf.parse_defs defs in
	merge_requirements_config c requirements

let path_t = Cmdliner.Arg.(value & (opt (some string) None) & info [ "config" ] ~doc:"Path to the configuration file.")

let pair3 a b c = (a, b, c)
let full_t =
	Cmdliner.Term.(const pair3 $ path_t $ options_t $ requirements_t)

let pair2 a b = (a, b)
let requirements_t =
	Cmdliner.Term.(const pair2 $ path_t $ requirements_t)

let from_config_options_requirements c =
	let options = merge_options_config c
		{ dummy_control_structs_o = None
		; if_while_translation_o = None
		; useless_continues_o = None } in
	let requirements = merge_requirements_config c
		{ weak_loop_control_instrs_o = None
		; weak_return_o = None
		; weak_control_structs_o = None } in
	(options, requirements)

let save_target options =
	options.symmetry || options.insert_block_ids
