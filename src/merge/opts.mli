type options = {
	dummy_control_structs : bool;
	if_while_translation : bool;
	useless_continues : bool;
}
val options_conf : bool -> (string * Config.Conf.value_def) list

type options_target_modif = {
	symmetry : bool;
	insert_block_ids : bool;
}
val options_target_modif_conf : bool -> (string * Config.Conf.value_def) list

type requirements = {
	weak_loop_control_instrs : bool;
	weak_return : bool;
	weak_control_structs : bool;
}
val requirements_conf : bool -> (string * Config.Conf.value_def) list

val full_t : (string option *
	(bool * bool * bool * bool * bool * bool * bool * bool * bool * bool * bool * bool) *
	(bool * bool * bool * bool * bool * bool))
		Cmdliner.Term.t

val requirements_t : (string option * (bool * bool * bool * bool * bool * bool)) Cmdliner.Term.t

val get_all : string option *
	(bool * bool * bool * bool * bool * bool * bool * bool * bool * bool * bool * bool) *
	(bool * bool * bool * bool * bool * bool) ->
	options * options_target_modif * requirements

val get_requirements : string option * (bool * bool * bool * bool * bool * bool) -> requirements

val from_config_options_requirements : Config.Conf.c -> (options * requirements)

val save_target : options_target_modif -> bool
