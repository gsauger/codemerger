open Defs.Base
open Utils
open Opts

let rec contains_continue_or_break p =
	let p_cf = match p with Prog (_, p_cf) -> p_cf in
	match p_cf with
	| If (_, if_body, if_tail) ->
		contains_continue_or_break if_body || contains_continue_or_break if_tail
	| While (_, _, while_tail) ->
		contains_continue_or_break while_tail
	| Continue
	| Break -> true
	| Return _
	| End -> false

let if_ (cond, body, tail) = If (cond, body, tail)
let while_ (cond, body, tail) = While (cond, body, tail)

(* Main recursive algorithm. Finds a way to map the graph structure of t into
   s, and create the corresponding merged graph s' along the way (None if no
   match was found).
   The contraint is that s' should behave like s when executed, so we allow only
   2 edits on s:
    - insertion (throught neutralizeprog)
    - substitution (not needed yet as we only have While and If nodes)
   If "symmetric" is ON, we allow ourselves to also modify t  if necessary
   This is a backtracking algorithm that stops as soon as a match is found.
   We progress through the two graphs at the same time and compare the nodes.
   Different decisions are taken depending on the result of the comparison. *)
let merge (source, source_ret) (target, target_ret) options options_tm requirements =

	let neutralize_data data =
		if options_tm.insert_block_ids then
			let id = Util.generate_new_block_id_asm () in
			([ id ; "__asm__(\"# __merger_dead_code\")" ], id :: data )
		else
			([ "__asm__(\"# __merger_dead_code\")" ], data) in

	let return_neutralize_prog data add_data cf1 cf2 =
		let (new_data1, new_data2) = neutralize_data data in
		Some (Prog (new_data1 @ add_data, cf1), Prog (new_data2, cf2)) in

	(* Keep a program structure but make it compile in all contexts. If the resulting
	   program may be executed, [strict] must be [true] to ensure that it does
	   nothing. [loop] means that the program will be the body of a loop, allowing
	   to end with a [continue] even in strict mode. *)
	let rec neutralize_prog p ret strict =
		let p_data, p_cf = match p with Prog (p_data, p_cf) -> (p_data), p_cf in
		match p_cf with
		| If (p_if_data, p1, p2) ->
			(* we have to neutralize p1 for the moment, in case it uses variables
			   that were defined before, but got neutralized *)
			(match (neutralize_prog p1 ret false, neutralize_prog p2 ret strict) with
			| (Some (new_p1_1, new_p1_2), Some (new_p2_1, new_p2_2)) ->
				let inst, cond = Util.generate_new_cond false in
				return_neutralize_prog p_data inst (If (cond, new_p1_1, new_p2_1)) (If (p_if_data, new_p1_2, new_p2_2))
			| _ -> None)
		| While (p_while_data, p1, p2) ->
			(match (neutralize_prog p1 ret false, neutralize_prog p2 ret strict) with
			| (Some (new_p1_1, new_p1_2), Some (new_p2_1, new_p2_2)) ->
				let inst, cond = Util.generate_new_cond false in
				return_neutralize_prog p_data inst (While (cond, new_p1_1, new_p2_1)) (While (p_while_data, new_p1_2, new_p2_2))
			| _ -> None)
		| End -> return_neutralize_prog p_data [] p_cf p_cf
		| Return _inst -> if strict then None else
			(match ret with
			| Defs.Types.Int -> return_neutralize_prog p_data [] (Return "12") p_cf
			| Defs.Types.Ptr -> return_neutralize_prog p_data [] (Return "(void*) 0") p_cf
			| Defs.Types.Void -> return_neutralize_prog p_data [] (Return "") p_cf)
		| Continue -> if strict then None else
			return_neutralize_prog p_data [] p_cf p_cf
		| Break -> if strict then None else
			return_neutralize_prog p_data [] p_cf p_cf in

	let match_return (source, target) =
		if options_tm.insert_block_ids then
			let s_data, s_cf = match source with Prog (s_data, s_cf) -> s_data, s_cf in
			let t_data, t_cf = match target with Prog (t_data, t_cf) -> t_data, t_cf in
			let id = Util.generate_new_block_id_asm () in
				Some (Prog (id :: s_data, s_cf), Prog (id :: t_data, t_cf))
		else
			Some (source, target) in

	let rec merge_aux (s, s_ret) (t, t_ret) sym (s_loop, t_loop) =
		let s_data, s_cf = match s with Prog (s_data, s_cf) -> s_data, s_cf in
		let _t_data, t_cf = match t with Prog (t_data, t_cf) -> t_data, t_cf in
		match s_cf, t_cf with
		| Continue, Continue
		| End, End
		| Break, Break
		| Return _, Return _ -> match_return (s, t)
		| If _, If _
		| While _, While _ ->
			match_successful (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| While _, If _ ->
			if requirements.weak_control_structs then
				match_successful (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
			else
				match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| If (s_if_cond, s_if_body, s_if_tail), While (_t_while_cond, _t_while_body, _t_if_body) ->
			if requirements.weak_control_structs then
				match_successful (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
			else if options.if_while_translation && (not (contains_continue_or_break s_if_body)) then
				let new_var = Util.generate_new_id () in
				let new_data = s_data @ [ (Printf.sprintf "int %s = 1" new_var) ] in
				let new_cond = Printf.sprintf "!(!(%s) | !(%s))" (* and *) s_if_cond new_var in
				let new_body =
					match s_if_body with
					| Prog (b, cf) -> Prog ((Printf.sprintf "%s = 0" new_var) :: b, cf) in
				(match merge_aux (Prog (new_data, While (new_cond, new_body, s_if_tail)), s_ret) (t, t_ret) sym (s_loop, t_loop) with
				| Some (s, t) -> Some (s, t)
				| None -> match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop))
			else
				match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| Continue, End ->
			if requirements.weak_loop_control_instrs then
				match_return (s, t)
			else if s_loop && options.useless_continues then
				match_return (Prog (s_data, End), t)
			else
				match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| End, Continue ->
			if requirements.weak_loop_control_instrs then
				match_return (s, t)
			else if s_loop && options.useless_continues then
				match_return (Prog (s_data, Continue), t)
			else
				match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| Break, End
		| Break, Continue
		| End, Break
		| Continue, Break ->
			if requirements.weak_loop_control_instrs then
				match_return (s, t)
			else
				match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| Return _, End
		| End, Return _ ->
			if requirements.weak_return then
				match_return (s, t)
			else
				match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| Return _, Continue
		| Return _, Break
		| Continue, Return _
		| Break, Return _ ->
			if requirements.weak_loop_control_instrs || requirements.weak_return then
				match_return (s, t)
			else
				match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)
		| If _, Return _
		| If _, Continue
		| If _, Break
		| If _, End
		| While _, Return _
		| While _, Continue
		| While _, Break
		| While _, End
		| Return _, If _
		| Return _, While _
		| Continue, If _
		| Continue, While _
		| Break, If _
		| Break, While _
		| End, If _
		| End, While _ ->
			match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop)

	and match_successful (s, s_ret) (t, t_ret) sym (s_loop, t_loop) =
		let s_data, s_cf, new_s_loop, s_cond, s_body, s_tail =
			match s with
			| Prog (s_data, If (s_cond, s_body, s_tail)) -> s_data, if_, false, s_cond, s_body, s_tail
			| Prog (s_data, While (s_cond, s_body, s_tail)) -> s_data, while_, true, s_cond, s_body, s_tail
			| _ -> assert false in
		let t_data, t_cf, new_t_loop, t_cond, t_body, t_tail =
			match t with
			| Prog (t_data, If (t_cond, t_body, t_tail)) -> t_data, if_, false, t_cond, t_body, t_tail
			| Prog (t_data, While (t_cond, t_body, t_tail)) -> t_data, while_, true, t_cond, t_body, t_tail
			| _ -> assert false in
		(match merge_aux (s_body, s_ret) (t_body, t_ret) true (new_s_loop, new_t_loop) with
		| Some (s_body_new, t_body_new) ->
			(match merge_aux (s_tail, s_ret) (t_tail, t_ret) true (s_loop, t_loop) with
			| Some (s_tail_new, t_tail_new) ->
				match_return (Prog (s_data, s_cf (s_cond, s_body_new, s_tail_new)),
				              Prog (t_data, t_cf (t_cond, t_body_new, t_tail_new)))
			| None -> match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop))
		| None -> match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop))

	and match_failed (s, s_ret) (t, t_ret) sym (s_loop, t_loop) =
		let res = try_dummy_cs (s, s_ret) (t, t_ret) (s_loop, t_loop) in
		match res with
		| Some res -> match_return res
		| None ->
			if options_tm.symmetry && sym then
				let res = merge_aux (t, t_ret) (s, s_ret) false (t_loop, s_loop) in
				Option.map (fun (a, b) -> (b, a)) res
			else
				None

	(* Mutually recursive function (with prog_match) that is called after
	   a "no match". It tries to match s whith the body or tail of the cs
	   of t. If it does, it inserts an dummy cs in s. A dummy cs is an always
	   true of false if or while, depending on which cs is in t, and if the
	   match was with its body or tail. *)
	and try_dummy_cs (s, s_ret) (t, t_ret) (s_loop, t_loop) =
		if options.dummy_control_structs then
			let s_data, s_cf = match s with Prog (s_data, s_cf) -> s_data, s_cf in
			let t_data, t_cf = match t with Prog (t_data, t_cf) -> t_data, t_cf in
			match t_cf with
			| If (t_cond, t_body, t_tail) ->
				let matched_s = merge_aux (s, s_ret) (t_body, t_ret) true (s_loop, false) in
				let neutralized_t_tail = neutralize_prog t_tail s_ret true in
				(match (matched_s, neutralized_t_tail) with
				| Some (new_s, new_t_body), Some (neutralized_t_tail, orig_t_tail) ->
					let inst, cond = Util.generate_new_cond true in
					Some (Prog (inst, If (cond, new_s, neutralized_t_tail)),
						Prog (t_data, If (t_cond, new_t_body, orig_t_tail)))
				| _ ->
					let matched_s = merge_aux (s, s_ret) (t_tail, t_ret) true (s_loop, t_loop) in
					let neutralized_t_body = neutralize_prog t_body s_ret false in
					(match (matched_s, neutralized_t_body) with
					| Some (new_s, new_t_tail), Some (neutralized_t_body, orig_t_body) ->
						let inst, cond = Util.generate_new_cond false in
						Some (Prog (inst, If (cond, neutralized_t_body, new_s)),
							Prog (t_data, If (t_cond, orig_t_body, new_t_tail)))
					| _ -> None))
			| While (t_cond, t_body, t_tail) ->
				let matched_s = merge_aux (s, s_ret) (t_body, t_ret) true (s_loop, true) in
				let neutralized_t_tail = neutralize_prog t_tail s_ret true in
				(match (matched_s, neutralized_t_tail) with
				| Some (new_s, new_t_body), Some (neutralized_t_tail, orig_t_tail) ->
					let new_id = Util.generate_new_id () in
					let new_data = [ (Printf.sprintf "int %s = 0" new_id) ] in
					Some (Prog (new_data, While (new_id ^ "++ == 0", new_s, neutralized_t_tail)),
						Prog (t_data, While (t_cond, new_t_body, orig_t_tail)))
						(* While loop with condition "true once then false" *)
				| _ ->
					let matched_s = merge_aux (s, s_ret) (t_tail, t_ret) true (s_loop, t_loop) in
					let neutralized_t_body = neutralize_prog t_body s_ret false in
					(match (matched_s, neutralized_t_body) with
					| Some (new_s, new_t_tail), Some (neutralized_t_body, orig_t_body) ->
						let inst, cond = Util.generate_new_cond false in
						Some (Prog (inst, While (cond, neutralized_t_body, new_s)),
							Prog (t_data, While (t_cond, orig_t_body, new_t_tail)))
					| _ -> None))
			| Return _
			| Continue
			| Break ->
				(* If symmetry is enabled, we can insert a return, continue or break
				   in s, inside a dummy if with always false condition. In order to
				   keep s and t in match, we also need to insert a dummy if in t,
				   this time always true, which is why we need symmetry for this case. *)
				if options_tm.symmetry then
					match s_cf with
					| End ->
						let inst_s, cond_s = Util.generate_new_cond false in
						let inst_t, cond_t = Util.generate_new_cond true in
						(match neutralize_prog t s_ret false with
						| Some (neutralized_t, orig_t) ->
							let new_s = Prog (s_data @ inst_s, If (cond_s, neutralized_t, Prog ([], End))) in
							let new_t = Prog (t_data @ inst_t, If (cond_t, orig_t, Prog ([], End))) in
							Some (new_s, new_t)
						| _ -> None)
					| _ -> None
				else
					None
			| End -> None
		else
			None
	in

	merge_aux (source, source_ret) (target, target_ret) true (false, false)
