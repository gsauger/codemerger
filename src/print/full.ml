open Defs.Full

let rec pp_rec first fmt p =
	let p_data, p_cf = match p with Prog (s_data, p_cf) -> s_data, p_cf in
	let () = if List.length p_data != 0 && not first then
		Format.fprintf fmt "@ " in
	let () = Format.fprintf fmt "%a" Instr.pp_instructions p_data in
	match p_cf with
	| Block (body, tail) ->
		Format.fprintf fmt "@ @[<v 2>{%a@]@ }%a" (pp_rec false) body (pp_rec false) tail
	| IfElse ((if_cond, if_body), elseif_list, else_body_o, tail) ->
		let () = Format.fprintf fmt "@ @[<v 2>if (%a) {%a@]@ }" Instr.pp_str if_cond (pp_rec false) if_body in
		let () =
			List.iter (fun (elseif_cond, elseif_body) ->
				Format.fprintf fmt "@ @[<v 2>else if (%a) {%a@]@ }" Instr.pp_str elseif_cond (pp_rec false) elseif_body)
			elseif_list in
		let () =
			match else_body_o with
			| Some else_body -> Format.fprintf fmt "@ @[<v 2>else {%a@]@ }" (pp_rec false) else_body
			| None -> () in
		Format.fprintf fmt "%a" (pp_rec false) tail
	| While (cond, body, tail) ->
		Format.fprintf fmt "@ @[<v 2>while (%a) {%a@]@ }%a" Instr.pp_str cond (pp_rec false) body (pp_rec false) tail
	| DoWhile (cond, body, tail) ->
		Format.fprintf fmt "@ @[<v 2>do {%a@]@ } while (%a);%a" (pp_rec false) body Instr.pp_str cond (pp_rec false) tail
	| For (init, cond, incr, body, tail) ->
		Format.fprintf fmt "@ @[<v 2>for (%a ; %a ; %a) {%a@]@ }%a"
			Instr.pp_str init Instr.pp_str cond Instr.pp_str incr (pp_rec false) body (pp_rec false) tail
	| Switch (exp, case_list, default_o, tail) ->
		let () = Format.fprintf fmt "@ @[<v 2>switch (%a) {" Instr.pp_str exp in
		let () =
			List.iter (fun (case_list, case_body) ->
				let len = List.length case_list in
				let () =
					List.iteri (fun num case_cond ->
						if num + 1 = len then
							Format.fprintf fmt "@ @[<v 2>case %a:" Instr.pp_str case_cond
						else
							Format.fprintf fmt "@ case %a:" Instr.pp_str case_cond)
					case_list in
				Format.fprintf fmt "%a@ break;@]" (pp_rec false) case_body)
			case_list in
		let () =
			match default_o with
			| Some default -> Format.fprintf fmt "@ @[<v 2>default:%a@ break;@]" (pp_rec false) default
			| None -> () in
		Format.fprintf fmt "@]@ }%a" (pp_rec false) tail
	| Return inst -> Format.fprintf fmt "@ return %a;" Instr.pp_str inst
	| Continue -> Format.fprintf fmt "@ continue;"
	| Break -> Format.fprintf fmt "@ break;"
	| End -> ()

(* pretty print a program *)
let pp fmt p =
	Format.fprintf fmt "@[<v>%a@]" (pp_rec true) p
