let pp_str fmt str =
	Format.fprintf fmt "%s" str

let rec pp_instruction fmt i =
	match i with
	| "{" -> Format.fprintf fmt "{"
	| "}" -> Format.fprintf fmt "}"
	| _ -> Format.fprintf fmt "%a;" pp_str i

and pp_instructions fmt l =
	let rec list_ppf_aux fmt l =
		match l with
		| [] -> ()
		| [ a ] -> Format.fprintf fmt "%a" pp_instruction a
		| a :: tl ->
			let () = Format.fprintf fmt "%a@ " pp_instruction a in
			list_ppf_aux fmt tl in
	Format.fprintf fmt "%a" list_ppf_aux l
