val pp_str : Format.formatter -> string -> unit
val pp_instruction : Format.formatter -> Defs.Instr.instruction -> unit
val pp_instructions : Format.formatter -> Defs.Instr.instructions -> unit
