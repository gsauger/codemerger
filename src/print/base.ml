open Defs.Base

let rec pp_rec first fmt p =
	let p_data, p_cf = match p with Prog (s_data, p_cf) -> s_data, p_cf in
	let () = if List.length p_data != 0 && not first then
		Format.fprintf fmt "@ " in
	let () = Format.fprintf fmt "%a" Instr.pp_instructions p_data in
	match p_cf with
	| While (cond, body, tail) ->
		Format.fprintf fmt "@ @[<v 2>while (%a) {%a@]@ }%a" Instr.pp_str cond (pp_rec false) body (pp_rec false) tail
	| If (cond, body, tail) ->
		Format.fprintf fmt "@ @[<v 2>if (%a) {%a@]@ }%a" Instr.pp_str cond (pp_rec false) body (pp_rec false) tail
	| Return inst ->
		Format.fprintf fmt "@ return %a;" Instr.pp_str inst
	| Continue ->
		Format.fprintf fmt "@ continue;"
	| Break ->
		Format.fprintf fmt "@ break;"
	| End -> ()

(* pretty print a program *)
let pp fmt p =
	Format.fprintf fmt "@[<v>%a@]" (pp_rec true) p
