open Defs.Full

let generate fuel =
	let generate_data in_case vars =
		let generate_instruction vars () =
			let r = Random.int (if in_case then 2 else 3) in
			match r with
			| 0 ->
				let i = Random.int vars in
				let v = (Random.int 100) + 1 in
				let instr = Printf.sprintf "var%i += %i" i v in
				(vars, instr)
			| 1 ->
				let i = Random.int vars in
				let instr = Printf.sprintf "printf(\"var%i = %%i\\n\", var%i)" i i in
				(vars, instr)
			| 2 ->
				let v = (Random.int 100) + 1 in
				let instr = Printf.sprintf "int var%i = %i" vars v in
				(vars + 1, instr)
			| _ -> assert false in
		let n = Random.int 20 in
		List.fold_left_map generate_instruction vars (List.init n (fun _ -> ())) in

	let generate_cond vars =
		let i = Random.int vars in
		let v = Random.int 2000 in
		Printf.sprintf "var%i > %i" i v in

	let rec generate_block in_loop in_case in_switch vars fuel =
		let body = generate_prog in_loop false in_switch vars (fuel - 1) in
		let tail = generate_prog in_loop in_case in_switch vars (fuel - 1) in
		Block (body, tail)

	and generate_ifelse in_loop in_case in_switch vars fuel =
		let generate_if () =
			let if_cond = generate_cond vars in
			let if_body = generate_prog in_loop false in_switch vars (fuel - 1) in
			(if_cond, if_body) in
		let if_ = generate_if () in
		let n = Random.int 3 in
		let elseif_list = List.map generate_if (List.init n (fun _ -> ())) in
		let else_ =
			if Random.bool () then (Some (generate_prog in_loop false in_switch vars (fuel - 1))) else None in
		let tail = generate_prog in_loop in_case in_switch vars (fuel - 1) in
		IfElse (if_, elseif_list, else_, tail)

	and generate_while in_loop in_case in_switch vars fuel =
		let cond = generate_cond vars in
		let body = generate_prog true false false vars (fuel - 1) in
		let tail = generate_prog in_loop in_case in_switch vars (fuel - 1) in
		While (cond, body, tail)

	and generate_dowhile in_loop in_case in_switch vars fuel =
		let cond = generate_cond vars in
		let body = generate_prog true false false vars (fuel - 1) in
		let tail = generate_prog in_loop in_case in_switch vars (fuel - 1) in
		DoWhile (cond, body, tail)

	and generate_for in_loop in_case in_switch vars fuel =
		let v = (Random.int 100) + 1 in
		let init = Printf.sprintf "int var%i = %i" vars v in
		let cond = generate_cond vars in
		let v = (Random.int 100) + 1 in
		let incr = Printf.sprintf "var%i += %i" vars v in
		let body = generate_prog true false false (vars + 1) (fuel - 1) in
		let tail = generate_prog in_loop in_case in_switch vars (fuel - 1) in
		For (init, cond, incr, body, tail)

	and generate_switch in_loop in_case in_switch vars fuel =
		let generate_case v () =
			let n = (Random.int 5) + 1 in
			let (new_v, case_list) =
				List.fold_left_map
					(fun v () ->
						let case = (Random.int 5) + 1 + v in
						(case, Printf.sprintf "%i" case))
					v (List.init n (fun _ -> ())) in
					let case_body = generate_prog in_loop true true vars (fuel - 1) in
			(new_v, (case_list, case_body)) in
		let n = Random.int 10 in
		let (_, case_list) = List.fold_left_map generate_case 0 (List.init n (fun _ -> ())) in
		let i = Random.int vars in
		let expr = Printf.sprintf "var%i" i in
		let default =
			if Random.bool () then (Some (generate_prog in_loop true true vars (fuel - 1))) else None in
		let tail = generate_prog in_loop in_case in_switch vars (fuel - 1) in
		Switch (expr, case_list, default, tail)

	and generate_cf in_loop in_case in_switch vars fuel =
		if fuel = 0 then
			let r = Random.int (if in_loop then (if in_switch then 2 else 3) else 1) in
			match r with
			| 0 -> End
			| 1 -> Continue
			| 2 -> Break
			| _ -> assert false
		else
			let r = Random.int 6 in
			match r with
			| 0 -> generate_block in_loop in_case in_switch vars fuel
			| 1 -> generate_ifelse in_loop in_case in_switch vars fuel
			| 2 -> generate_while in_loop in_case in_switch vars fuel
			| 3 -> generate_dowhile in_loop in_case in_switch vars fuel
			| 4 -> generate_for in_loop in_case in_switch vars fuel
			| 5 -> generate_switch in_loop in_case in_switch vars fuel
			| _ -> assert false
	
		and generate_prog in_loop in_case in_switch vars fuel =
			let (vars, data) = generate_data in_case vars in
			let cf = generate_cf in_loop in_case in_switch vars fuel in
			Prog (data, cf) in

	generate_prog false false false 1 fuel
