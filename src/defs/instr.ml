(* type instructions represents code that is a sequence of instructions
   i.e. without control flow instruction *)

type instruction = string

type instructions = instruction list
