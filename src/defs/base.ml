open Instr

(* type cf represents a control flow instruction, with
   data * prog * prog being condition * body * tail
   Note: End is a leaf

   We only have While and If cf nodes for now in our model.
   For can be added later, as it similar to While.
   An If/Else statement can be decomposed in several Ifs.

   examples in C:
     if(data){body;} tail;
     while(data){body;} tail;
*)

type cf =
	| If of instruction * prog * prog
	| While of instruction * prog * prog
	| Return of instruction
	| Break
	| Continue
	| End

(* We recursively define a program as some code (data)
   and a control flow instruction (that may be End) *)
and prog =
	| Prog of instructions * cf
