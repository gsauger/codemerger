open Instr

type cf =
	| Block of prog * prog
	| IfElse of (instruction * prog) * (instruction * prog) list * prog option * prog
	| While of instruction * prog * prog
	| DoWhile of instruction * prog * prog
	| For of instruction * instruction * instruction * prog * prog (* var_decl, cond, incr, body, tail *)
	| Switch of instruction * (instruction list * prog) list * prog option * prog
	| Return of instruction
	| Break
	| Continue
	| End

and prog =
	| Prog of instructions * cf
