open Utils

let parse fpos fcode =
	let code_buf = Sedlexing.Utf8.from_string fcode in
	let () = Sedlexing.set_filename code_buf fpos.Lexing.pos_fname in
	let () = Sedlexing.set_position code_buf fpos in
	try
		MenhirLib.Convert.Simplified.traditional2revised Parser.funct
			(fun () ->
				let tok = Lexer.lex code_buf in
				let (pstart, pend) = Sedlexing.lexing_positions code_buf in
				let () = Logs.debug (fun m ->
					let pstart_c = pstart.Lexing.pos_cnum - pstart.Lexing.pos_bol in
					let pend_c = pend.Lexing.pos_cnum - pend.Lexing.pos_bol in
					let pos_string = Printf.sprintf "%i:%i -> %i:%i"
						pstart.Lexing.pos_lnum pstart_c pend.Lexing.pos_lnum pend_c in
						m "[In FUN parser] pos %s %stoken : %a%!" pos_string (Util.spaces (18 - (String.length pos_string))) Lexer.pp_tok tok) in
				(tok, pstart, pend))
	with
	| Parser.Error -> let (pstart, pend) = Sedlexing.lexing_positions code_buf in
		Error.e (Error.ParseError (pstart, pend))
	| e -> raise e
