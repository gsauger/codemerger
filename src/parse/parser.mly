%token FOR WHILE DO IF ELSE SWITCH CASE CONTINUE BREAK DEFAULT RETURN LPAREN RPAREN LBRACKET RBRACKET SEMI COLON
%token<string> CODE (* type *)
%token<string> ID

%start <Defs.Full.prog> funct (* axiom *)

%nonassoc IF
%nonassoc ELSE

%%

funct:
| LBRACKET code = block_content RBRACKET { code }

block_common(cs):
| LBRACKET b = block_content RBRACKET { b }
| code = instruction { Defs.Full.Prog ([ code ], Defs.Full.End) }
| cs = cs { Defs.Full.Prog ([], cs (Defs.Full.Prog ([], Defs.Full.End))) }

block:
| b = block_common(control_struct_no_block) { b }

block_no_if:
| b = block_common(control_struct_no_if_no_block) { b }

block_content:
| data = instructions cs = control_struct tail = block_content { Defs.Full.Prog (data, cs tail) }
| data = instructions { Defs.Full.Prog (data, Defs.Full.End) }

block_content_no_break:
| data = instructions cs = control_struct_no_break tail = block_content_no_break { Defs.Full.Prog (data, cs tail) }
| data = instructions { Defs.Full.Prog (data, Defs.Full.End) }

control_struct:
| i = if_block { fun tail -> let (if_code, if_cont, else_code) = i in Defs.Full.IfElse (if_code, if_cont, else_code, tail) }
| BREAK SEMI { fun _ -> Defs.Full.Break }
| b = scope_block { let body = b in fun tail -> Defs.Full.Block (body, tail) }
| cs = control_struct_common { cs }

control_struct_no_block:
| i = if_block { fun tail -> let (if_code, if_cont, else_code) = i in Defs.Full.IfElse (if_code, if_cont, else_code, tail) }
| BREAK SEMI { fun _ -> Defs.Full.Break }
| cs = control_struct_common { cs }

control_struct_no_if_no_block:
| BREAK SEMI { fun _ -> Defs.Full.Break }
| cs = control_struct_common { cs }

control_struct_no_break:
| i = if_block { fun tail -> let (if_code, if_cont, else_code) = i in Defs.Full.IfElse (if_code, if_cont, else_code, tail) }
| b = scope_block { let body = b in fun tail -> Defs.Full.Block (body, tail) }
| cs = control_struct_common { cs }

control_struct_common:
| b = while_block { let (cond, code) = b in fun tail -> Defs.Full.While (cond, code, tail) }
| b = dowhile_block SEMI { let (cond, code) = b in fun tail -> Defs.Full.DoWhile (cond, code, tail) }
| b = for_block { let (init, cond, incr, code) = b in fun tail -> Defs.Full.For (init, cond, incr, code, tail) }
| b = switch_block { let (exp, sb, default) = b in fun tail -> Defs.Full.Switch (exp, sb, default, tail) }
| CONTINUE SEMI { fun _ -> Defs.Full.Continue }
| RETURN i = instruction_aux SEMI { fun _ -> Defs.Full.Return ( i ) }

if_block:
| IF LPAREN cond = instruction_aux RPAREN code = block elseif = if_block_cont { let (if_cont, else_code) = elseif in ((cond, code), if_cont, else_code) }

if_block_cont:
| ELSE IF LPAREN cond = instruction_aux RPAREN code = block cont = if_block_cont { let (if_cont, else_code) = cont in ((cond, code) :: if_cont, else_code) }
| ELSE code = block_no_if { [], Some code }
| { [], None } %prec IF

while_block:
| WHILE LPAREN cond = instruction_aux RPAREN code = block { (cond, code) }

dowhile_block:
| DO code = block WHILE LPAREN cond = instruction_aux RPAREN { (cond, code) }

for_block:
| FOR LPAREN init = instruction_aux SEMI cond = instruction_aux SEMI incr = instruction_aux RPAREN code = block { (init, cond, incr, code ) }

switch_block:
| SWITCH LPAREN exp = instruction_aux RPAREN LBRACKET sb = switch_body RBRACKET { let (cl, sd) = sb in (exp, cl, sd) }

switch_body:
| e = switch_elem BREAK SEMI sb = switch_body { let (el, sd) = sb in (e :: el, sd) }
| e = switch_elem { ([ e ], None) }
| d = switch_default BREAK SEMI { ([], Some d) }
| d = switch_default { ([], Some d) }
| { ([], None) }

switch_elem:
| cl = switch_case_list code = block_content_no_break { (cl, code) }

switch_case_list:
| CASE cond = instruction_aux_no_colon COLON cl = switch_case_list { cond :: cl }
| CASE cond = instruction_aux_no_colon COLON { [ cond ] }

switch_default:
| DEFAULT COLON code = block_content_no_break { code }

scope_block:
| LBRACKET b = block_content RBRACKET { b }

instructions:
| i = instruction il = instructions { i :: il }
| { [] }

instruction:
| i = instruction_aux SEMI { i }

instruction_aux:
| c = CODE d = instruction_aux { c ^ " " ^ d }
| id = ID d = instruction_aux { id ^ " " ^ d }
| COLON d = instruction_aux { ": " ^ d }
| LPAREN d1 = instruction_aux RPAREN d2 = instruction_aux { "(" ^ d1 ^ ")" ^ d2 }
| { "" }

instruction_aux_no_colon:
| c = CODE d = instruction_aux_no_colon { c ^ " " ^ d }
| id = ID d = instruction_aux_no_colon { id ^ " " ^ d }
| LPAREN d1 = instruction_aux RPAREN d2 = instruction_aux_no_colon { "(" ^ d1 ^ ")" ^ d2 }
| { "" }
