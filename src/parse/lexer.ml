let rec lex buf =
	match%sedlex buf with
	| '#', Star ((Sub (any, ('\n' | '/')) | ('/', Sub (any, '*')) | "\\\n")) -> Parser.CODE (Sedlexing.Utf8.lexeme buf)
	| "//", Star (Sub (any, '\n')), '\n'
	| "/*", Star (Sub (any, '*') | '*', Sub (any, '/')) , "*/"
	| ' ' | '\t' | '\n' | '\013' | '\012' -> lex buf
	| "for" -> Parser.FOR
	| "while" -> Parser.WHILE
	| "do" -> Parser.DO
	| "if" -> Parser.IF
	| "else" -> Parser.ELSE
	| "switch" -> Parser.SWITCH
	| "case" -> Parser.CASE
	| "continue" -> Parser.CONTINUE
	| "break" -> Parser.BREAK
	| "default" -> Parser.DEFAULT
	| "return" -> Parser.RETURN
	| "(" -> Parser.LPAREN
	| ")" -> Parser.RPAREN
	| "{" -> Parser.LBRACKET
	| "}" -> Parser.RBRACKET
	| ";" -> Parser.SEMI
	| ":" -> Parser.COLON
	| id_start, (Star id_continue) -> Parser.ID (Sedlexing.Utf8.lexeme buf)
	| '"', Star ((Sub (any, ('"' | '\\'))) | '\\', any), '"' -> Parser.CODE (Sedlexing.Utf8.lexeme buf)
	| '\'', (any | ('\\', any, Star (Sub (any, '\'')))), '\'' -> Parser.CODE (Sedlexing.Utf8.lexeme buf)
	| '/' -> Parser.CODE (Sedlexing.Utf8.lexeme buf)
	| Plus (Sub (any, (' ' | '\t' | '\n' | '\013' | '(' | ')' | '{' | '}' | ';' | ':' | '"' | '\'' | '/'))) -> Parser.CODE (Sedlexing.Utf8.lexeme buf)
	| eof -> assert false
	| _ -> assert false

let pp_tok fmt tok =
	let tok_str =
		match tok with
		| Parser.FOR -> "FOR"
		| Parser.WHILE -> "WHILE"
		| Parser.DO -> "DO"
		| Parser.IF -> "IF"
		| Parser.ELSE -> "ELSE"
		| Parser.SWITCH -> "SWITCH"
		| Parser.CASE -> "CASE"
		| Parser.CONTINUE -> "CONTINUE"
		| Parser.BREAK -> "BREAK"
		| Parser.DEFAULT -> "DEFAULT"
		| Parser.RETURN -> "RETURN"
		| Parser.LPAREN -> "LPAREN ("
		| Parser.RPAREN -> "RPAREN )"
		| Parser.LBRACKET -> "LBRACKET {"
		| Parser.RBRACKET -> "RBRACKET }"
		| Parser.SEMI -> "SEMI ;"
		| Parser.COLON -> "COLON :"
		| Parser.CODE s -> Printf.sprintf "CODE \"%s\"" s
		| Parser.ID s -> Printf.sprintf "ID \"%s\"" s in
	Format.fprintf fmt "%s " tok_str
