%token COLON NEW_LINE EOF
%token<string> ID STRING
%token<bool> BOOL

%start<(string * Defs.value * (Lexing.position * Lexing.position) * (Lexing.position * Lexing.position)) list> config

%%

config:
| l = line c = config { l :: c }
| NEW_LINE c = config { c }
| EOF { [] }

line:
| id = ID COLON s = STRING NEW_LINE { (id, Defs.String s, $loc(id), $loc(s)) }
| id = ID COLON b = BOOL NEW_LINE { (id, Defs.Bool b, $loc(id), $loc(b)) }
