let rec lex buf =
	match%sedlex buf with
	| '#', Star ((Sub (any, '\n') | "\\\n"))
	| ' ' | '\t' -> lex buf
	| '\n' -> Parser.NEW_LINE
	| ':' -> Parser.COLON
	| "true" -> Parser.BOOL true
	| "false" -> Parser.BOOL false
	| '"', Star (Sub (any, ('"' | '\\' | '\n')) | "\\", any), '"' ->
		let t = Sedlexing.Utf8.lexeme buf in
		Parser.STRING (String.sub t 1 ((String.length t) - 2))
	| "\"\"\"", Star (Sub (any, ('"' | '\\')) | "\\", any | '"', (Sub (any, '"') | '"', (Sub (any, '"')))), "\"\"\"" ->
		let t = Sedlexing.Utf8.lexeme buf in
		Parser.STRING (String.sub t 3 ((String.length t) - 6))
	| id_start, Star id_continue -> Parser.ID (Sedlexing.Utf8.lexeme buf)
	| eof -> Parser.EOF
	| _ -> let (pstart, pend) = Sedlexing.lexing_positions buf in
			Utils.Error.e (Utils.Error.LexError (pstart, pend))
