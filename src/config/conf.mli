type value_def =
	| String of string option
	| Bool of bool option

type d
type c

val init : (string * value_def) list -> d
val parse_find : d -> string -> int -> c
val parse_defs : d -> c
val get_val_string : c -> string -> string * (Lexing.position * Lexing.position) option
val get_val_bool : c -> string -> bool * (Lexing.position * Lexing.position) option
