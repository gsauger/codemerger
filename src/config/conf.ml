module StringMap = Map.Make(String)

let type_str v =
	match v with
	| Defs.String _ -> "a string"
	| Defs.Bool _ -> "a boolean"

type value_def =
	| String of string option
	| Bool of bool option

let type_str_defs vd =
	match vd with
	| String _ -> "a string"
	| Bool _ -> "a boolean"

type d = (value_def) StringMap.t
type c = (Defs.value * (Lexing.position * Lexing.position) option) StringMap.t

let init values =
	StringMap.of_list values

let rec find path fuel =
	try
		In_channel.open_bin path
	with
	| _ ->
		if fuel <= 0 then
			Utils.Error.e Utils.Error.CantFindConfigFile
		else
			find ("../" ^ path) (fuel - 1)

let parse config_defs code_buf =
	let conf_temp =
		try
			MenhirLib.Convert.Simplified.traditional2revised Parser.config
				(fun () ->
					let tok = Lexer.lex code_buf in
					let (pstart, pend) = Sedlexing.lexing_positions code_buf in
					let () = Logs.debug (fun m ->
						let pstart_c = pstart.Lexing.pos_cnum - pstart.Lexing.pos_bol in
						let pend_c = pend.Lexing.pos_cnum - pend.Lexing.pos_bol in
						let pos_string = Printf.sprintf "%i:%i -> %i:%i"
							pstart.Lexing.pos_lnum pstart_c pend.Lexing.pos_lnum pend_c in
							m "[In CONFIG parser] pos %s" pos_string) in
					(tok, pstart, pend))
		with
		| _ -> let (pstart, pend) = Sedlexing.lexing_positions code_buf in
			Utils.Error.e (Utils.Error.ParseError (pstart, pend)) in
	let conf_map_temp = List.fold_left
		(fun res (key, value, pos_key, pos_value) ->
			match StringMap.find_opt key res with
			| Some _ -> Utils.Error.e (Utils.Error.ConfigFoundTwice (key, pos_value))
			| None ->
				match (value, StringMap.find_opt key config_defs) with
				| (Defs.String _, Some (String _)) -> StringMap.add key (value, Some pos_key) res
				| (Defs.Bool _, Some (Bool _)) -> StringMap.add key (value, Some pos_key) res
				| (got, Some exp) ->
					Utils.Error.e (InvalidConfigType (key, type_str got, type_str_defs exp, pos_value))
				| (_, None) -> Utils.Error.e (Utils.Error.UnknownConfig (key, pos_key)))
		StringMap.empty conf_temp in
	StringMap.fold (fun key def_value_o res ->
		match StringMap.find_opt key res with
		| Some _ -> res
		| None ->
			(match def_value_o with
			| String (Some s) -> StringMap.add key (Defs.String s, None) res
			| Bool (Some b) -> StringMap.add key (Defs.Bool b, None) res
			| _ -> Utils.Error.e (Utils.Error.MissingConfig key)))
		config_defs conf_map_temp

let parse_find config_defs config_file fuel =
	let in_ch = find config_file fuel in
	let buf = Sedlexing.Utf8.from_channel in_ch in
	let () = Sedlexing.set_filename buf config_file in
	let config = parse config_defs buf in config

let parse_defs config_defs =
	let buf = Sedlexing.Utf8.from_string "" in
	let config = parse config_defs buf in config

let get_val_string config key =
	match StringMap.find_opt key config with
	| Some (Defs.String s, pos) -> (s, pos)
	| _ -> assert false

let get_val_bool config key =
	match StringMap.find_opt key config with
	| Some (Defs.Bool b, pos) -> (b, pos)
	| _ -> assert false
