open Utils

let version =
	match Build_info.V1.version () with
	| None -> "n/a"
	| Some v -> Build_info.V1.Version.to_string v

let f_set_size formatter =
	try
		let terminal_width, _ = ANSITerminal.size () in
		Format.pp_set_margin formatter terminal_width
	with Failure _ ->
		let () = Format.pp_set_margin formatter (max_int - 1) in
		Logs.warn (fun m -> m "Impossible to get terminal size")

let copts debug seed style_renderer level =
	let style_renderer = style_renderer in
	let () = Fmt_tty.setup_std_outputs ?style_renderer () in
	let () = Logs.set_level ~all:true level in
	let () = if debug then Printexc.record_backtrace true in
	let () =
		match seed with
		| None -> Random.self_init ()
		| Some seed -> Random.init seed in
	()


let copts_t =
	let debug = Cmdliner.Arg.(value & flag & info [ "d"; "debug" ]
		~doc:"Enable debug mode: record and print backtraces of uncaught exceptions.") in
	let seed = Cmdliner.Arg.(value & (opt (some int) None) & info [ "seed" ]
		~doc:"Sets the seed used to initialize the random number generator.") in
	Cmdliner.Term.(const copts $ debug $ seed $ Fmt_cli.style_renderer () $ Logs_cli.level ())

let merge () source_arg target_arg inplace inject output merge_config =
	try
		let (options, options_target_modif, requirements) =
			Merge.Opts.get_all merge_config in

		let parse_arg arg arg_name =
			let () = if arg = "" then Error.e (Error.ArgMissing arg_name) in
			let l = String.split_on_char ':' arg in
			match l with
			| [ file ; funct ] -> (file, funct)
			| _ -> Error.e (Error.InvalidFormat (arg_name, None)) in
		let (source_path, source_func) = parse_arg source_arg "SOURCE" in
		let (target_path, target_func) = parse_arg target_arg "TARGET" in

		let () = Logs.debug (fun m->m "Parsing source and target ...") in
		let source = new File.File_class.file (Path source_path) in
		let target = new File.File_class.file (Path target_path) in
		let () = Logs.debug (fun m->m "Done ...") in

		let source_func = source#get_fun source_func in
		let target_func = target#get_fun target_func in

		let merged = Merge.Base.merge (source_func#get_base, source_func#get_ret) (target_func#get_base, target_func#get_ret) options options_target_modif requirements in

		match merged with
		| None -> Error.e Error.MergeImpossible
		| Some (s_merged, t_merged) ->
			let () = source_func#set_base s_merged in
			let () =
				if inject then
					target_func#set_base s_merged
				else
					target_func#set_base t_merged in
			let () =
				if inject then
					Logs.app (fun m -> m "%a" target_func#pp File.Func_class.Auto)
				else
					let () = Logs.app (fun m -> m "%a" source_func#pp File.Func_class.Auto) in
					if Merge.Opts.save_target options_target_modif then
						Logs.app (fun m -> m "@[--------------------------------\n%a@]" target_func#pp File.Func_class.Auto) in
			let () =
				if inplace then
					let () = if not inject then source#save File.Func_class.Auto in
					if Merge.Opts.save_target options_target_modif || inject then target#save File.Func_class.Auto in
			let () =
				match output with
				| Some output ->
					if Merge.Opts.save_target options_target_modif then
						let l = String.split_on_char ';' output in
						let (source_output, target_output) =
							match l with
							| [ source_output ; target_output ] -> (source_output, target_output)
							| _ -> Error.e (Error.InvalidFormat ("PATH", None)) in
						let () = source#save_as File.Func_class.Auto source_output in
						let () = target#save_as File.Func_class.Auto target_output in
						()
					else
						if inject then
							target#save_as File.Func_class.Auto output
						else
							source#save_as File.Func_class.Auto output
				| None -> () in
			()

	with
	| e -> Error.print_error e

let merge_t =
	let doc = "Try to merge [SOURCE] into [TARGET] and print the resulting program. The format of [SOURCE] and [TARGET] is \"file.c:function\"." in
	let source = Cmdliner.Arg.(value & (pos 0 string "") & info [] ~docv:"SOURCE") in
	let target = Cmdliner.Arg.(value & (pos 1 string "") & info [] ~docv:"TARGET") in
	let inplace = Cmdliner.Arg.(value & flag & info [ "i"; "in-place" ] ~doc:"If set, the input files will be saved in-place with the result of the merge.") in
	let inject = Cmdliner.Arg.(value & flag & info [ "inject" ] ~doc:"If set, the merged source code will be injected in the target function.") in
	let output = Cmdliner.Arg.(value & (opt (some string) None) & info [ "o"; "output" ]
		~doc:"If set, the modified input files will be saved with name [PATH]. If symmetric mode or block ids are enabled,
					the format of [PATH] must be \"source_output.c;target_output.c\"." ~docv:"PATH") in
	Cmdliner.Cmd.(
		v (info "merge" ~doc)
		Cmdliner.Term.(const merge $ copts_t $ source $ target $ inplace $ inject $ output $ Merge.Opts.full_t))

let verify () source_arg target_arg merge_config =
	try
		let requirements = Merge.Opts.get_requirements merge_config in

		let parse_arg arg arg_name =
			let () = if arg = "" then Error.e (Error.ArgMissing arg_name) in
			let l = String.split_on_char ':' arg in
			match l with
			| [ file ; funct ] -> (file, funct)
			| _ -> Error.e (Error.InvalidFormat (arg_name, None)) in
		let (source_path, source_func) = parse_arg source_arg "SOURCE" in
		let (target_path, target_func) = parse_arg target_arg "TARGET" in

		let () = Logs.debug (fun m->m "Parsing source and target ...") in
		let source = new File.File_class.file (Path source_path) in
		let target = new File.File_class.file (Path target_path) in
		let () = Logs.debug (fun m->m "Done ...") in

		let source_func = source#get_fun source_func in
		let target_func = target#get_fun target_func in

		let options = Merge.Opts.
			{ dummy_control_structs = false
			; if_while_translation = false
			; useless_continues = false } in
		let options_target_modif = Merge.Opts.
			{ symmetry = false
			; insert_block_ids = false } in

		let result = Merge.Base.merge (source_func#get_base, source_func#get_ret) (target_func#get_base, target_func#get_ret) options options_target_modif requirements in

		match result with
		| None -> Error.e Error.DontMatch
		| Some _ -> ()

	with
	| e -> Error.print_error e

let verify_t =
	let doc = "Verify if [SOURCE] is successfully merged into [TARGET]. The format of [SOURCE] and [TARGET] is \"file.c:function\"." in
	let source = Cmdliner.Arg.(value & (pos 0 string "") & info [] ~docv:"SOURCE") in
	let target = Cmdliner.Arg.(value & (pos 1 string "") & info [] ~docv:"TARGET") in
	Cmdliner.Cmd.(
		v (info "verify" ~doc)
		Cmdliner.Term.(const verify $ copts_t $ source $ target $ Merge.Opts.requirements_t))

type format =
	| Full
	| Base

let parse () source format inplace output count =
	try
		let parse_arg arg arg_name =
			let () = if arg = "" then Error.e (Error.ArgMissing arg_name) in
			let l = String.split_on_char ':' arg in
			match l with
			| [ file ] -> (file, None)
			| [ file ; funct ] -> (file, Some funct)
			| _ -> Error.e (Error.InvalidFormat (arg_name, None)) in
		let (source_path, source_func) = parse_arg source "SOURCE" in
		let () = if source_func = None && count then
			Error.e Error.CountNoFunction in

		let printing_mode =
			match format with
			| Full -> File.Func_class.Full
			| Base -> File.Func_class.Base in

		let file = new File.File_class.file (Path source_path) in
		let dummy_buff = Buffer.create 20 in
		let dummy_form = Format.formatter_of_buffer dummy_buff in
		let () = match source_func with
		| Some func ->
			let func = file#get_fun func in
			if count then
				let n1,n2,n3,n4,n5,n6,n7,n8 = func#get_number_cf printing_mode in
				Logs.app (fun m -> m "%s,%i,%i,%i,%i,%i,%i,%i,%i" func#get_name n1 n2 n3 n4 n5 n6 n7 n8)
			else
				let () = Format.fprintf dummy_form "%a" func#pp printing_mode in
				Logs.app (fun m -> m "%a" func#pp printing_mode)
		| None ->
			let () = Format.fprintf dummy_form "%a" file#pp printing_mode in
			Logs.app (fun m -> m "%a" file#pp printing_mode) in

		let () =
			if inplace then
				file#save File.Func_class.Auto in

		let () = match output with
			| Some path -> file#save_as File.Func_class.Auto path
			| None -> () in

		()
	with
	| e -> Error.print_error e

let parse_t =
	let doc = "Try to parse [SOURCE] and print the functions in requested format. The format of [SOURCE] may be \"file.c:function\",\
						 in this case only the requested function is parsed and printed, or \"file.c\", in this case the whole file is parsed and printed." in
	let source = Cmdliner.Arg.(value & (pos 0 string "") & info [] ~docv:"SOURCE") in
	let format_enum = [("full", Full) ; ("base" , Base)] in
	let format_doc = Format.asprintf "Select format of output. $(docv) must be %s." (Cmdliner.Arg.doc_alts_enum format_enum) in
	let format = Cmdliner.Arg.(value & (opt (enum format_enum) Base) & info [ "f"; "format" ] ~docv:"FMT" ~doc:format_doc) in
	let inplace = Cmdliner.Arg.(value & flag & info [ "i"; "in-place" ] ~doc:"If set, the input file will be saved in-place with the result of the parse.") in
	let count = Cmdliner.Arg.(value & flag & info [ "c"; "count" ] ~doc:"If set, only prints function_name, (#if, #else, #cases (switch), #while, #dowhile, #for, #return, #break/continue") in
	let output = Cmdliner.Arg.(value & (opt (some string) None) & info [ "o"; "output" ] ~doc:"If set, the parsed input file will be saved with name [PATH]." ~docv:"PATH") in
	Cmdliner.Cmd.(
		v (info "parse" ~doc)
		Cmdliner.Term.(const parse $ copts_t $ source $ format $ inplace $ output $ count))

let fuzz () fuel output =
	try
		let f = Fuzz.Full.generate fuel in

		let func = new File.Func_class.func "fuzz" Void "void fuzz(int var0)" (FullProg f) in
		let fe_list =
			[ File.Func_class.Code "#include <stdio.h>\n \
													 void fuzz(int);\n \
													 int main(void)\n \
													 {\n \
														 fuzz(0);\n \
														 return 0;\n \
													 }\n"
			; File.Func_class.Func func ] in
		let file = new File.File_class.file (CList fe_list) in
		let () = 
			Logs.app (fun m -> m "%a" file#pp Full) in

		let () = match output with
			| Some path -> file#save_as Auto path
			| None -> () in
		()
	with
	| e -> Error.print_error e


let fuzz_t =
	let doc = "Generate random programs." in
	let output = Cmdliner.Arg.(value & (opt (some string) None) & info [ "o"; "output" ] ~doc:"If set, the generated program will be saved with name [PATH]." ~docv:"PATH") in
	let fuel = Cmdliner.Arg.(value & (opt int 5) & info [ "f"; "fuel" ] ~doc:"Sets the fuel quantity given to the fuzzer, more fuel will give bigger programs.") in
	Cmdliner.Cmd.(
		v (info "fuzz" ~doc)
		Cmdliner.Term.(const fuzz $ copts_t $ fuel $ output))

let list_funs_names () source =
	try
		let () = if source = "" then Error.e (Error.ArgMissing "SOURCE") in
		let file = new File.File_class.file (Path source) in
		List.iter (fun f -> Logs.app (fun m -> m "%s" f)) file#get_fun_names
	with
	| e -> Error.print_error e

let list_t =
	let doc = "Try to parse [SOURCE] and print the name of all functions." in
	let source = Cmdliner.Arg.(value & (pos 0 string "") & info [] ~docv:"SOURCE") in
	Cmdliner.Cmd.(
		v (info "list" ~doc )
		Cmdliner.Term.(const list_funs_names $ copts_t $ source)) (* fun $ arg1 $ arg2 ) *)

let () =
	let () = f_set_size Format.std_formatter in
	let () = f_set_size Format.err_formatter in
	let () = Logs.set_reporter (Logs_fmt.reporter ()) in
	let doc = "Kelpie is a tool which reads c programs and modifies the source code while preserving the semantic." in
	let inf = Cmdliner.Cmd.info ~version ~doc "kelpie" in
	let default = Cmdliner.Term.(ret (const (fun _ -> `Help (`Pager, None)) $ copts_t)) in
	let cmd = Cmdliner.Cmd.group inf ~default [ merge_t ; verify_t ; parse_t ; fuzz_t ; list_t ] in
		Cmdliner.Cmd.(exit @@ eval cmd)
