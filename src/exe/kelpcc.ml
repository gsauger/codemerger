let parse_arg arg =
	let l = String.split_on_char ':' arg in
	match l with
	| [ file ; funct ] -> Some (file, funct)
	| _ -> None

let generate_payload payload_raw (payload_pstart, _payload_pend) =
	let decl = "int fun(void){" in
	let payload_str = decl ^ payload_raw ^ "}" in
	let pstart = { payload_pstart with Lexing.pos_cnum = payload_pstart.Lexing.pos_cnum - String.length decl } in
		(new File.File_class.file (String (payload_str, Some pstart)), "fun")

let execute_compiler compiler remove_input remove_output add_args input =
	let args_list = Array.to_list Sys.argv in
	let (args_list, _) =
		if remove_output then
			List.fold_left (fun (al, remove) arg ->
				if remove then (al, false)
				else
					if String.starts_with ~prefix:"-o" arg then
						if arg = "-o" then (al, true) else (al, false)
				else (arg::al, false)) ([], false) args_list
		else (args_list, false) in
	let args_list = if remove_output then List.rev args_list else args_list in
	let args =
		Array.of_list ((if remove_input
			then List.filter (fun arg -> not (String.ends_with ~suffix:".c" arg)) args_list
			else args_list) @ add_args) in
	let () = args.(0) <- compiler in
	let () = Logs.app (fun m -> m "%a" Utils.Util.pp_arr args) in
	let (gcc_stdout, gcc_stdin) = Unix.open_process_args compiler args in
	let pid = Unix.process_pid (gcc_stdout, gcc_stdin) in
	let () = Out_channel.output_string gcc_stdin input in
	let () = Out_channel.close gcc_stdin in
	let output = In_channel.input_all gcc_stdout in
	let () = In_channel.close gcc_stdout in
	let (_, pstatus) = Unix.waitpid [] pid in
	match pstatus with
	| WEXITED 0 -> output
	| WEXITED i -> exit i
	| _ -> exit 1

let execute_merger merge_options merge_requirements compiler (target_file, target_fun) payload_raw payload_pos =
	let pp_target = execute_compiler compiler false true [ "-E" ; "-o" ; "-" ] "" in

	let target = new File.File_class.file (String (pp_target, None)) in

	let (payload, payload_fun) =
		match parse_arg payload_raw with
		| Some (payload_file, payload_fun) ->
			if String.ends_with ~suffix:".c" payload_file then
				(new File.File_class.file (Path payload_file), payload_fun)
			else
				generate_payload payload_raw payload_pos
		| None ->
				generate_payload payload_raw payload_pos in

	let payload_func = payload#get_fun payload_fun in
	let target_func = target#get_fun target_fun in

	let merge_options_target_modif = Merge.Opts.
		{ symmetry = false
		; insert_block_ids = false } in

	let () = Logs.app (fun m -> m "kelpie merge - %s:%s" target_file target_fun) in
	let result = Merge.Base.merge (payload_func#get_base, payload_func#get_ret) (target_func#get_base, target_func#get_ret) merge_options merge_options_target_modif merge_requirements in

	match result with
	| None -> Utils.Error.e Utils.Error.MergeImpossible
	| Some (merged_payload, _) ->
		let () = target_func#set_base merged_payload in
		let inj_target = Format.asprintf "%a" target#pp File.Func_class.Auto in
		let output = execute_compiler compiler true false ["-x" ; "cpp-output" ; "-"] inj_target in
		if output <> "" then Logs.app (fun m -> m "%s" output)

let config_defs = Merge.Opts.(options_conf true @ requirements_conf false @
	[ ("compiler", Config.Conf.String (Some "gcc"))
	; ("target", Config.Conf.String None)
	; ("payload", Config.Conf.String None) ])

let () =
	let () = Fmt_tty.setup_std_outputs ~style_renderer:`Ansi_tty ~utf_8:true () in
	let () = Logs.set_reporter (Logs_fmt.reporter ()) in

	try
		let c = Config.Conf.parse_find (Config.Conf.init config_defs) ".kelpccrc" 12 in

		let (compiler, _) = Config.Conf.get_val_string c "compiler" in

		let (target, target_pos_o) = Config.Conf.get_val_string c "target" in
		let (target_file, target_fun) =
			match parse_arg target with
			| Some res -> res
			| None -> Utils.Error.e (Utils.Error.InvalidFormat ("target", target_pos_o)) in

		let (payload, payload_pos_o) = Config.Conf.get_val_string c "payload" in
		let payload_pos = match payload_pos_o with Some p -> p | None -> assert false in

		let (merge_options, merge_requirements) = Merge.Opts.from_config_options_requirements c in

		match Array.find_opt (String.ends_with ~suffix:".c") Sys.argv with
		| Some file when file = target_file -> execute_merger merge_options merge_requirements compiler (target_file, target_fun) payload payload_pos
		| _ -> let output = execute_compiler compiler false false [] "" in
			if output <> "" then Logs.app (fun m -> m "%s" output)
	with
	| e -> Utils.Error.print_error e
