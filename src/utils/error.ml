type err =
	| NoSuchFile of string
	| ImpossibleToWrite of string
	| CantFindConfigFile
	| MissingConfig of string
	| ConfigFoundTwice of string * (Lexing.position * Lexing.position)
	| UnknownConfig of string * (Lexing.position * Lexing.position)
	| InvalidConfigType of string * string * string * (Lexing.position * Lexing.position)
	| IncompatibleOptions of string * string
	| LexError of Lexing.position * Lexing.position
	| ParseError of Lexing.position * Lexing.position
	| MergeImpossible
	| InvalidFormat of string * ((Lexing.position * Lexing.position) option)
	| ArgMissing of string
	| FunctionNotFound of string * string
	| CountNoFunction
	| DontMatch

exception MergerError of err

let e err = raise (MergerError err)

let compute_comment_for_location (pos1, pos2) =
	let line2 = pos2.Lexing.pos_lnum in
	let col2 = pos2.Lexing.pos_cnum - pos2.Lexing.pos_bol in
	let pos1 = pos1 in
	let line1 = pos1.Lexing.pos_lnum in
	let col1 = pos1.Lexing.pos_cnum - pos1.Lexing.pos_bol in
	let msg =
		if line1 = line2 then
			if col1 = col2 then
				Printf.sprintf "line %d, character %d" line2 col2
			else
				Printf.sprintf "line %d, characters %d-%d" line2 col1 col2
		else
			Printf.sprintf "line %d, character %d to line %d, character %d" line1 col1
				line2 col2 in
	match pos1.Lexing.pos_fname with
	| "" -> msg
	| "<stdin>" -> Printf.sprintf "file <stdin>, %s" msg
	| fname -> Printf.sprintf "file \"%s\", %s" fname msg

let err_string_code err =
	match err with
	| NoSuchFile file -> (Printf.sprintf "Can't open file \"%s\" for reading" file, 123)
	| ImpossibleToWrite file -> (Printf.sprintf "Can't open file \"%s\" for writing" file, 123)
	| CantFindConfigFile -> ("Can't find config file", 123)
	| MissingConfig k -> (Printf.sprintf "Can't find key \"%s\" in config file" k, 123)
	| ConfigFoundTwice (k, pos) ->
			(Printf.sprintf "Duplicate config entry \"%s\" at %s" k (compute_comment_for_location pos), 123)
	| UnknownConfig (k, pos) ->
			(Printf.sprintf "Unknown config entry \"%s\" at %s" k (compute_comment_for_location pos), 123)
	| InvalidConfigType (k, got, exp, pos) ->
			(Printf.sprintf "Invalid type for config entry \"%s\", got %s but expecting %s at %s" k got exp (compute_comment_for_location pos), 123)
	| IncompatibleOptions (o1, o2) ->
			(Printf.sprintf "The options \"%s\" and \"%s\" are incompatible" o1 o2, 124)
	| LexError (pstart, pend) ->
			(Printf.sprintf "Lex error at %s" (compute_comment_for_location (pstart, pend)), 123)
	| ParseError (pstart, pend) ->
			(Printf.sprintf "Parse error at %s" (compute_comment_for_location (pstart, pend)), 123)
	| MergeImpossible -> ("Impossible to merge, no match found", 123)
	| InvalidFormat (arg, None) -> (Printf.sprintf "Bad format of argument %s" arg, 124)
	| InvalidFormat (arg, Some pos) ->
		(Printf.sprintf "Bad format of argument %s, at %s" arg (compute_comment_for_location pos), 124)
	| ArgMissing arg -> (Printf.sprintf "Argument %s is mandatory" arg, 124)
	| FunctionNotFound (file, func) -> (Printf.sprintf "Function \"%s\" not found in \"%s\"" func file, 123)
	| CountNoFunction -> (Printf.sprintf "Cannot use count without specifyng a function name", 124)
	| DontMatch -> ("These functions do not match", 123)

let pp_text fmt text =
	let words = String.split_on_char ' ' text in
	match words with
	| w :: words ->
			let () = Format.fprintf fmt "%s" w in
				List.iter (fun w -> Format.fprintf fmt "@ %s" w) words
	| _ -> ()

let print_error e =
	match e with
	| MergerError err ->
		let (msg, code) = err_string_code err in
		let () = Logs.err (fun m -> m "%a" pp_text msg) in
			exit code
	| Failure s ->
		let () = Logs.err (fun m -> m "Bug,@ please@ report:@ failure:@ %a" pp_text s) in
			exit 125
	| Assert_failure (file, line, col) ->
		let () = Logs.err (fun m -> m "Bug,@ please@ report:@ assertion@ failed@ at@ %s:%i:%i" file line col) in
			exit 125
	| e ->
		let bt = (Printexc.get_backtrace ()) in
		let () =
			if bt = "" then
				Logs.err (fun m -> m "Bug,@ please@ report:@ uncaught@ exception@ \"%s\"@ (run@ with@ \"-d\"@ to@ see@ backtrace)" (Printexc.to_string e))
			else
				Logs.err (fun m -> m "Bug,@ please@ report:@ uncaught@ exception@ \"%s\"\nBACKTRACE:\n%a" (Printexc.to_string e) pp_text bt) in
			exit 125
