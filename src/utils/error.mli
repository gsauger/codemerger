type err =
	| NoSuchFile of string
	| ImpossibleToWrite of string
	| CantFindConfigFile
	| MissingConfig of string
	| ConfigFoundTwice of string * (Lexing.position * Lexing.position)
	| UnknownConfig of string * (Lexing.position * Lexing.position)
	| InvalidConfigType of string * string * string * (Lexing.position * Lexing.position)
	| IncompatibleOptions of string * string
	| LexError of Lexing.position * Lexing.position
	| ParseError of Lexing.position * Lexing.position
	| MergeImpossible
	| InvalidFormat of string * ((Lexing.position * Lexing.position) option)
	| ArgMissing of string
	| FunctionNotFound of string * string
	| CountNoFunction
	| DontMatch

val e : err -> 'a

val print_error : exn -> 'a
