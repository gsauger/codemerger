let rec spaces i = if i <= 0 then "" else " " ^ (spaces (i - 1))

let ctr = ref 0 (* modifiable, static var *)
let generate_new_id () =
	let () = ctr := !ctr + 1 in
	Printf.sprintf "__merger_%i" !ctr

let cond_true = ref ""
let fun_decl = ref []

let generate_fun_call () =
	let id = generate_new_id () in
	let () = fun_decl := Printf.sprintf "void %s(void)" id :: !fun_decl in
	Printf.sprintf "%s()" id

let get_global_code () =
	if !cond_true = ""
		then !fun_decl
		else Printf.sprintf "int %s = 1" !cond_true :: !fun_decl

let generate_new_cond b =
	let () = if !cond_true = "" then cond_true := Printf.sprintf "__merger_cond_true_%i" (Random.int 1073741823) in
	let new_cond = match b with
	| true ->
			Printf.sprintf "%s" !cond_true
	| false ->
			Printf.sprintf "!(%s)" !cond_true
	in
	([], new_cond)

let contains s1 s2 =
	let re = Str.regexp_string s2 in
		try ignore (Str.search_forward re s1 0); true
		with Not_found -> false

let ctr2 = ref 0 (* modifiable, static var *)
let generate_new_block_id_asm () =
	let () = ctr := !ctr + 1 in
	Printf.sprintf "__asm__(\"# __merger_block_id %i\")" !ctr

let pp_list fmt l =
  let rec list_ppf_aux fmt l =
    match l with
    | [] -> ()
    | [ a ] -> Format.fprintf fmt "%s" a
    | a :: tl ->
        let () = Format.fprintf fmt "%s " a in
        list_ppf_aux fmt tl
  in
  Format.fprintf fmt "%a" list_ppf_aux l

let pp_arr fmt l =
	pp_list fmt (Array.to_list l)
